CREATE TRIGGER totalprice_Header
ON OrderConfirmationLine
FOR UPDATE
AS
BEGIN
 IF UPDATE (OrderStatus)
        BEGIN
		 INSERT INTO stock (
                PaperID
                ,GramsPerSquareMeter
                ,QuantityKilogram
                ,QuantitySheet
				,QuantityReam
                )
		SELECT PaperID
		,GramsPerSquareMeter
		,QuantityKilogram
        ,QuantitySheet
		,QuantityReam
		FROM inserted
		END
END
GO