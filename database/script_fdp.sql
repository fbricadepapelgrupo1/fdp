create database fdp
use fdp
CREATE TABLE Users (
	UserID INT NOT NULL IDENTITY ,
	UName VARCHAR(50) NOT NULL,
	Address1 VARCHAR(100) NOT NULL,
	Address2 VARCHAR(100) NOT NULL,
	City varchar(100) NOT NULL,
	PostalCode VARCHAR(10) NOT NULL,
	Country VARCHAR(50) NOT NULL,
	UserPassword VARCHAR(255) NOT NULL,
	UserName VARCHAR(255) NOT NULL,
	UserType INTEGER NOT NULL DEFAULT 0,
	PRIMARY KEY (UserID),
	--FOREIGN KEY (UserType) REFERENCES UserType(UserTypeID)
);

CREATE TABLE UserType (
	UserTypeID INT NOT NULL IDENTITY,
	UserTypeDescription VARCHAR(255) NOT NULL,

);

CREATE TABLE PaperType (
	PaperID INT NOT NULL IDENTITY,
	PaperType VARCHAR(50) NOT NULL,
	PaperLength INT NOT NULL,
	PaperWidth INT NOT NULL,
	GramsPerSquareMeter DECIMAL NOT NULL,
	PRIMARY KEY (PaperID)
);


CREATE TABLE Stock (
	StockID INT NOT NULL IDENTITY,
	PaperID INT NOT NULL,
	GramsPerSquareMeter INT NOT NULL,
	QuantityKilogram INT NOT NULL,
	QuantitySheet INT NOT NULL,
	QuantityReam INT NOT NULL,
	PRIMARY KEY (StockID),
	--FOREIGN KEY (PaperID) REFERENCES PaperType(PaperID)
);


CREATE TABLE OrderConfirmationHeader (
	OrderConfirmationID INT NOT NULL IDENTITY,
	OrderDate DATETIME NOT NULL,
	UserID INT NOT NULL,
	TotalOrderPrice DECIMAL NOT NULL,
	OrderForeignID varchar(50) NOT NULL
	PRIMARY KEY (OrderConfirmationID),
	--FOREIGN KEY (UserID) REFERENCES Users (UserID)
);


CREATE TABLE OrderConfirmationLine (
	OrderConfimationLineID INT NOT NULL IDENTITY,
	OrderConfirmationID INT NOT NULL,
	ProductIdentifierBuyer INT NOT NULL,
	ProductIdentifierSupplier INT NOT NULL,
	TaxPercent INT NOT NULL,
	TaxLocation VARCHAR(50) NOT NULL,
	TaxType VARCHAR(20) NOT NULL,
	PricePerUnit DECIMAL NOT NULL,
	QuantityReam INT,
	QuantitySheet INT,
	QuantityKilogram INT,
	TotalLinePrice DECIMAL NOT NULL,
	OrderStatus TINYINT NOT NULL DEFAULT 0,
	PaperID int NOT NULL,
	PRIMARY KEY (OrderConfimationLineID),
	FOREIGN KEY (OrderConfirmationID) REFERENCES OrderConfirmationHeader(OrderConfirmationID),
	FOREIGN KEY (PaperID) REFERENCES PaperType (PaperID),
	--FOREIGN KEY (ProductIdentifierBuyer) REFERENCES Stock(StockID)
);

--CREATE TABLE Sales(
	--SalesID INT NOT NULL IDENTITY,
	--PaperID INT NOT NULL,
	--GramsPerSquareMeter DECIMAL NOT NULL,
	--QuantityReam INT NOT NULL,
	--QuantitySheet INT NOT NULL,
	--QuantityKilogram INT NOT NULL,
	--TotalPrice DECIMAL NOT NULL
	--PRIMARY KEY (SalesID),
	--FOREIGN KEY (PaperID) REFERENCES PaperType (PaperID)
--);
