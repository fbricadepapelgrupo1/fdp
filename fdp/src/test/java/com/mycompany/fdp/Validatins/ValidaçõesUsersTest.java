/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.fdp.Validatins;

import validations.ValidationsUsers;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author rui
 */
public class ValidaçõesUsersTest {
    
    public ValidaçõesUsersTest() {
    }

    /**
     * Test of validarCampoVazio method, of class ValidationsUsers.
     */
    @Test
    public void testValidarCampoVazio() {
        System.out.println("validarCampoVazio");
        String word = "w";
        ValidationsUsers instance = new ValidationsUsers();
        boolean expResult = false;
        boolean result = instance.validarCampoVazioString(word);
        assertEquals(expResult, result);
    }

    /**
     * Test of validarCampoLetras method, of class ValidationsUsers.
     */
    @Test
    public void testValidarCampoLetras() {
        System.out.println("validarCampoLetras");
        String word = "WroDs";
        ValidationsUsers instance = new ValidationsUsers();
        boolean expResult = false;
        boolean result = instance.validarCampoLetras(word);
        assertEquals(expResult, result);
    }

    /**
     * Test of validarCampoNumeros method, of class ValidationsUsers.
     */
    @Test
    public void testValidarCampoNumeros() {
        System.out.println("validarCampoNumeros");
        String word = "123456789";
        ValidationsUsers instance = new ValidationsUsers();
        boolean expResult = false;
        boolean result = instance.validarCampoNumeros(word);
        assertEquals(expResult, result);
    }
    
}
