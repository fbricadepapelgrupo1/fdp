/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.fdp.Validatins;

import validations.xmlOrderValidation;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author rui
 */
public class xmlOrederValidationTest {
    
    public xmlOrederValidationTest() {
    }

    /**
     * Test of validar method, of class xmlOrederValidation.
     */
    @Test
    public void testValidarY() {
        System.out.println("validarYes");
        char input = 'Y';
        xmlOrderValidation instance = new xmlOrderValidation();
        int expResult = 1;
        int result = instance.validar(input);
        assertEquals(expResult, result);
    }
    /**
     * Test of validar method, of class xmlOrederValidation.
     */
    @Test
    public void testValidarN() {
        System.out.println("validarNO");
        char input = 'N';
        xmlOrderValidation instance = new xmlOrderValidation();
        int expResult = 2;
        int result = instance.validar(input);
        assertEquals(expResult, result);
    }
    /**
     * Test of validar method, of class xmlOrederValidation.
     */
    @Test
    public void testValidar() {
        System.out.println("validarDEFAULT");
        char input = '1';
        xmlOrderValidation instance = new xmlOrderValidation();
        int expResult = 0;
        int result = instance.validar(input);
        assertEquals(expResult, result);
    }
    
}
