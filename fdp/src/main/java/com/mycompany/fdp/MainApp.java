package com.mycompany.fdp;

import java.io.IOException;
import java.sql.SQLException;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.mycompany.fdp.api.ApiServer;
import com.mycompany.fdp.database.DB_Con;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

public class MainApp{
 //coverter de java para json e vice versa
    private static final ObjectMapper objectMapper = new ObjectMapper()
            .setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.NONE)
            .setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY)
            .enable(SerializationFeature.INDENT_OUTPUT);
    public static void main(String[] args) throws SQLException, SAXException, ParserConfigurationException, IOException, ClassNotFoundException {
       Login login1 = new Login();
       login1.login1();
       //DB.openConnection(); // abrir conexao a bd antes de tudo
       //new ApiServer(objectMapper,8080);

    }


}
