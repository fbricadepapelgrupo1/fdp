/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.fdp;

import com.mycompany.fdp.Model.Users;
import com.mycompany.fdp.database.DB_Con;
import com.mycompany.fdp.Model.OrderConfirmationHeader;
import com.mycompany.fdp.Model.Stock;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import static java.time.Clock.system;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import java.util.Scanner;

/**
 *
 * @author Rafael Granja
 */


public class Login {

    String userName;
    String userPassword;
    private PreparedStatement pst = null;
    Users sessao;

    public Login() throws SQLException {
    }

    /**
     * inicia a sessão e invoca o menu
     *
     * @throws SQLException
     * @throws ClassNotFoundException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     */
    public void login1() throws SQLException, ClassNotFoundException, ParserConfigurationException, SAXException, IOException {

        Scanner ler = new Scanner(System.in);
        DB_Con con = new DB_Con();
        Statement query1 = con.getConnection().createStatement();

        System.out.println("\nBem vindo à Feira & Paper");
        do {
            System.out.println("Username");
            userName = ler.nextLine();
        } while (userName == null || userName.isEmpty());

        do {
            System.out.println("Password");
            userPassword = ler.nextLine();
        } while (userPassword == null || userPassword.isEmpty());
        try {
            ResultSet rs1 = query1.executeQuery("Select * from Users where UserName= '" + userName + "'");
            while (rs1.next()) {
                if (rs1.getString("UserPassword").equals(userPassword)) {
                    sessao = new Users(rs1.getInt("UserID"), rs1.getString("UName"), rs1.getString("Address1"), rs1.getString("Address2"), rs1.getString("City"), rs1.getString("PostalCode"), rs1.getString("Country"), rs1.getString("UserPassword"), rs1.getString("UserName"), rs1.getInt("UserType"));
                    System.out.printf("Bem Vindo %s", userName);
                    if (rs1.getInt("UserType") == 0) {
                        loginFornecedor();
                    } else if (rs1.getInt("UserType") == 1) {
                        loginOperador();
                    } else if (rs1.getInt("UserType") == 2) {
                        loginAdmin();
                    } else {
                        System.out.println("Erro");
                    }
                } else {
                    System.out.println("UserName ou Password incorreta erro");
                    login1();
                }
            }
            login1();
        } catch (Exception e) {
            System.out.println("UserName ou Password incorreta");
            login1();
        }
    }

    public void loginAdmin() throws SQLException, ClassNotFoundException, ParserConfigurationException, SAXException, IOException {

        Scanner in = new Scanner(System.in);
        int escolher1;
        boolean flag;
        do {
            flag = false;
            try {
                System.out.println("\n 1-Gerir Fornecedor\n 2-Gerir Operador\n 3-Listar Stock\n 4-Apovar Stock\n 5-Consultar Conta Corrente\n 6-Fechar Programa");
                escolher1 = in.nextInt();
                switch (escolher1) {
                    case 1:
                        switch2();
                        flag = true;
                        escolher1 = 0;
                        break;
                    case 2:
                        switch1();
                        flag = true;
                        escolher1 = 0;
                        break;
                    case 3:
                        Stock stock = new Stock();
                        stock.listarStock();
                        flag = true;
                        escolher1 = 0;
                        break;
                    case 4:
                        //aprovar stock
                        flag = true;
                        escolher1 = 0;
                        break;
                    case 5:
                        //consultar conta corrente
                        flag = true;
                        escolher1 = 0;
                        break;
                    case 6:
                        System.exit(0);
                        break;
                    default:
                        flag = true;
                        escolher1 = 0;
                        break;
                }
            } catch (Exception e) {
                System.out.println("Erro valor não soportado " + e);
                loginAdmin();
            }
        } while (flag);
    }

    public void switch1() throws SQLException, ClassNotFoundException, SAXException, ParserConfigurationException, IOException {

        Scanner in = new Scanner(System.in);
        int escolher1;
        boolean flag;
        do {
            flag = false;
            try {
                System.out.println("\n 1-Criar Operador\n 2-Modificar Operador\n 3-Apagar Operador\n 4-Voltar Atrás");
                escolher1 = in.nextInt();
                switch (escolher1) {
                    case 1:
                        Users operador = new Users();
                        operador.operadorCriarFornecedor();
                        flag = true;
                        escolher1 = 0;
                        break;
                    case 2:
                        //modificar operador
                        flag = true;
                        escolher1 = 0;
                        Users.EditarUsers();
                        break;
                    case 3:
                        //apagar operador
                        flag = true;
                        escolher1 = 0;
                        Users.apagarUtilizador();
                        break;
                    case 4:
                        loginAdmin();
                        flag = true;
                        escolher1 = 0;
                        break;
                    default:
                        flag = true;
                        escolher1 = 0;
                        break;
                }
            } catch (Exception e) {
                System.out.println("Erro valor não soportado " + e);
                loginAdmin();
            }

        } while (flag);

    }

    public void switch2() throws SQLException, SAXException, ParserConfigurationException, IOException, ClassNotFoundException {

        Scanner in = new Scanner(System.in);
        int escolher1;
        boolean flag;
        do {
            flag = false;
            try {
                System.out.println("\n 1-Criar Fornecedor\n 2-Modificar Fornecedor\n 3-Apagar Fornecedor\n 4-Voltar Atrás");
                escolher1 = in.nextInt();
                switch (escolher1) {
                    case 1:
                        Users fornecedor = new Users();
                        fornecedor.adminCriarUtilizador();
                        flag = true;
                        escolher1 = 0;
                        break;
                    case 2:
                        //modificar fornecedor
                        flag = true;
                        escolher1 = 0;
                        Users.EditarUsers();
                        break;
                    case 3:
                        //apagar fornecedor
                        flag = true;
                        escolher1 = 0;
                        Users.apagarUtilizador();
                        break;
                    case 4:
                        loginAdmin();
                        flag = true;
                        escolher1 = 0;
                        break;
                    default:
                        flag = true;
                        escolher1 = 0;
                        break;
                }
            } catch (Exception e) {
                System.out.println("Erro valor não soportado " + e);
                loginAdmin();
            }

        } while (flag);

    }

    public void loginFornecedor() throws SQLException, SAXException, ParserConfigurationException, IOException {

        Scanner in = new Scanner(System.in);
        int escolher1;
        boolean flag;
        do {
            flag = false;
            try {
                System.out.println("\n 1-Importar Xml\n 2-Fechar Programa");
                escolher1 = in.nextInt();
                switch (escolher1) {
                    case 1:
                        XML xml = new XML();
                        xml.loadXMLtoBD();
                        flag = true;
                        escolher1 = 0;
                        break;
                    case 2:
                        System.exit(0);
                        break;
                    default:
                        flag = true;
                        escolher1 = 0;
                        break;
                }
            } catch (Exception e) {
                System.out.println("Erro valor não soportado " + e);
                loginFornecedor();
            }

        } while (flag);

    }

    public void loginOperador() throws SQLException, ClassNotFoundException, SAXException, ParserConfigurationException, IOException {

        Scanner in = new Scanner(System.in);
        int escolher1;
        boolean flag;
        do {
            flag = false;
            try {
                System.out.println("\n 1-Aprovar Stock\n 2-Criar Fornecedor\n 3-Consultar Conta Corrente\n 4-Listar Stock\n 5-Fechar Programa");
                escolher1 = in.nextInt();
                switch (escolher1) {
                    case 1:
                        //aprova stock
                        flag = true;
                        escolher1 = 0;
                        break;
                    case 2:
                        Users fornecedor = new Users();
                        fornecedor.adminCriarUtilizador();
                        flag = true;
                        escolher1 = 0;
                        break;
                    case 3:
                        //consultar conta corrente
                        flag = true;
                        escolher1 = 0;
                        break;
                    case 4:
                        Stock stock = new Stock();
                        stock.listarStock();
                        flag = true;
                        escolher1 = 0;
                        break;
                    case 5:
                        System.exit(0);
                        break;
                    default:
                        flag = true;
                        escolher1 = 0;
                        break;
                }
            } catch (Exception e) {
                System.out.println("Erro valor não soportado " + e);
                loginOperador();
            }
        } while (flag);
    }
}
