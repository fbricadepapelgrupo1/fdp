package com.mycompany.fdp;

import com.mycompany.fdp.database.DB_Con;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import validations.XMLvalidations;

public class XML {

    public static double soma;
    public static int headerid;

    public static XMLvalidations validar = new XMLvalidations();
    public static File fXmlFile;
    public static String filename;

    /**
     * Este método lê o ficheiro XML e mostra os dados no ecrã
     */
    public void lerXML() {

        try {
            Scanner read = new Scanner(System.in);  // Create a Scanner object
            System.out.println("O ficheiro deve ser colocado na pasta 'XML' ");
            System.out.println("Nome do ficheiro:");
            String filename = read.nextLine();

            File fXmlFile = new File("/XML/" + filename + ".xml");
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);

            doc.getDocumentElement().normalize();

            NodeList lista1 = doc.getElementsByTagName("OrderConfirmationHeader");

            for (int temp = 0; temp < lista1.getLength(); temp++) {

                Node nNode = lista1.item(temp);

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                    Element eElement = (Element) nNode;

                    System.out.println("Dados Cabeçalho :");

                    System.out.println("Year :" + Integer.valueOf(eElement.getElementsByTagName("Year").item(0).getTextContent()));
                    System.out.println("Month : " + Integer.valueOf(eElement.getElementsByTagName("Month").item(0).getTextContent()));
                    System.out.println("Day : " + Integer.valueOf(eElement.getElementsByTagName("Day").item(0).getTextContent()));
                    System.out.println("Party Identifier : " + Integer.valueOf(eElement.getElementsByTagName("PartyIdentifier").item(0).getTextContent()));
                    System.out.println("Name : " + eElement.getElementsByTagName("Name").item(0).getTextContent());
                    System.out.println("Address1 : " + eElement.getElementsByTagName("Address1").item(0).getTextContent());
                    System.out.println("Address2 : " + eElement.getElementsByTagName("Address2").item(0).getTextContent());
                    System.out.println("City :" + eElement.getElementsByTagName("City").item(0).getTextContent());
                    System.out.println("PostalCode : " + eElement.getElementsByTagName("PostalCode").item(0).getTextContent());
                    System.out.println("Country : " + (eElement.getElementsByTagName("Country").item(0).getTextContent()));
                    System.out.println("ISO CountryCode : " + eElement.getElementsByTagName("Country").item(0).getAttributes().item(0).getTextContent());
                }
            }
            System.out.println("----------------------------------------------------------------------------------------------");

            NodeList lista2 = doc.getElementsByTagName("OrderConfirmationLineItem");

            for (int temp = 0; temp < lista2.getLength(); temp++) {

                Node nNode = lista2.item(temp);

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;

                    System.out.println("Dados Linhas da Encomenda:");
                    System.out.println("--------------------------------------------------------------------");
                    System.out.println("Linha :" + Integer.valueOf(eElement.getElementsByTagName("OrderConfirmationLineItemNumber").item(0).getTextContent()));

                    System.out.println("Buyer ProductIdentifier : " + Integer.valueOf(eElement.getElementsByTagName("ProductIdentifier").item(0).getTextContent()));
                    System.out.println("Supplier ProductIdentifier : " + Integer.valueOf(eElement.getElementsByTagName("ProductIdentifier").item(1).getTextContent()));
                    System.out.println("Length : " + eElement.getElementsByTagName("Value").item(0).getTextContent());
                    System.out.println("Width : " + eElement.getElementsByTagName("Value").item(1).getTextContent());
                    System.out.println("GramsPerSquareMeter : " + eElement.getElementsByTagName("DetailValue").item(0).getTextContent());

                    System.out.println("Currency Value : " + Double.valueOf(eElement.getElementsByTagName("CurrencyValue").item(0).getTextContent()));
                    System.out.println("Currency Type :" + eElement.getElementsByTagName("CurrencyValue").item(0).getAttributes().item(0).getTextContent());
                    System.out.println(eElement.getElementsByTagName("Value").item(2).getAttributes().item(0).getTextContent());
                    System.out.println(Double.valueOf(eElement.getElementsByTagName("CurrencyValue").item(1).getTextContent()));
                    System.out.println(eElement.getElementsByTagName("CurrencyValue").item(1).getAttributes().item(0).getTextContent());
                    System.out.println(Integer.valueOf(eElement.getElementsByTagName("MonetaryAdjustmentLine").item(0).getTextContent()));
                    System.out.println(eElement.getElementsByTagName("MonetaryAdjustment").item(0).getAttributes().item(0).getTextContent());
                    System.out.println(Integer.valueOf(eElement.getElementsByTagName("TaxPercent").item(0).getTextContent()));
                    System.out.println(eElement.getElementsByTagName("TaxAdjustment").item(0).getAttributes().item(0).getTextContent());
                    System.out.println(eElement.getElementsByTagName("TaxAdjustment").item(0).getAttributes().item(1).getTextContent());
                    System.out.println(Double.valueOf(eElement.getElementsByTagName("CurrencyValue").item(2).getTextContent()));
                    System.out.println(eElement.getElementsByTagName("TaxLocation").item(0).getTextContent());

                    System.out.println(eElement.getElementsByTagName("InformationalQuantity").item(1).getAttributes().item(0).getTextContent());
                    System.out.println(eElement.getElementsByTagName("Value").item(5).getAttributes().item(0).getTextContent());
                    System.out.println(Double.valueOf(eElement.getElementsByTagName("Value").item(5).getTextContent()));

                    System.out.println(eElement.getElementsByTagName("InformationalQuantity").item(0).getAttributes().item(0).getTextContent());
                    System.out.println(eElement.getElementsByTagName("Value").item(4).getAttributes().item(0).getTextContent());
                    System.out.println(Double.valueOf(eElement.getElementsByTagName("Value").item(4).getTextContent()));

                    System.out.println(eElement.getElementsByTagName("Quantity").item(0).getAttributes().item(0).getTextContent());
                    System.out.println(eElement.getElementsByTagName("Value").item(3).getAttributes().item(0).getTextContent());
                    System.out.println(Double.valueOf(eElement.getElementsByTagName("Value").item(3).getTextContent()));
                    System.out.println("------------------------------------------------------------------------------");
                    double TotalLinePrice = (Double.valueOf(eElement.getElementsByTagName("CurrencyValue").item(1).getTextContent()));

                    //Somar total das linhas para saber total da order
                    soma = soma + TotalLinePrice;
                    

                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Método para inserir o ficheiro XML na base de dados
     *
     * @throws SQLException
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     */
    public void loadXMLtoBD() throws SQLException, SAXException, ParserConfigurationException, IOException {

        try {
            Scanner read = new Scanner(System.in);  // Create a Scanner object
            System.out.println("Confirme o nome do ficheiro:");

            filename = read.nextLine();
            validar.validarFileName(filename);
            fXmlFile = new File("/XML/" + filename + ".xml");
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);

            doc.getDocumentElement().normalize();

            NodeList nList = doc.getElementsByTagName("OrderConfirmationHeader");
            System.out.println("A ler o ficheiro " + fXmlFile);
            System.out.println("------------------------------------------------------------------------------------------------------------------");

            //INSERIR CABEÇALHO
            for (int temp = 0; temp < nList.getLength(); temp++) {

                Node nNode = nList.item(temp);

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                    Element eElement = (Element) nNode;

                    String statusOrder = eElement.getAttribute("OrderConfirmationStatusType");

                    String dateOrder = eElement.getElementsByTagName("Year").item(0).getTextContent()
                            + eElement.getElementsByTagName("Month").item(0).getTextContent()
                            + eElement.getElementsByTagName("Day").item(0).getTextContent();
                    int dateYear = Integer.valueOf(eElement.getElementsByTagName("Year").item(0).getTextContent());
                    int dateMonth = Integer.valueOf(eElement.getElementsByTagName("Month").item(0).getTextContent());
                    int dateDay = Integer.valueOf(eElement.getElementsByTagName("Day").item(0).getTextContent());

                    String ForeignID = eElement.getElementsByTagName("OrderConfirmationReference").item(0).getTextContent();

                    String fornName = eElement.getElementsByTagName("Name").item(0).getTextContent();
                    String fornAddress1 = eElement.getElementsByTagName("Address1").item(0).getTextContent();
                    String fornAddress2 = eElement.getElementsByTagName("Address2").item(0).getTextContent();
                    String fornCity = eElement.getElementsByTagName("City").item(0).getTextContent();
                    String fornPostal = eElement.getElementsByTagName("PostalCode").item(0).getTextContent();
                    String fornCountry = eElement.getElementsByTagName("Country").item(0).getTextContent();
                    
                    //Validar se a encomenda já existe na BD @JLeal
                    validar.existeEncomenda(ForeignID);
                    //Validação de fornecedor registado na BD
                    validar.existeFornecedor(fornName, fornPostal);
                    //Validar Data
                    validar.validarDataXML(dateDay, dateMonth, dateYear);
                    //Validar Cod.Postal
                    //validar.validarCodPostal(fornPostal);
                    Statement stm = DB_Con.getConnection().createStatement();
                    String cabecalho = "INSERT INTO OrderConfirmationHeader (OrderDate, UserID, TotalOrderPrice, OrderForeignID) VALUES ('" + dateOrder + "', 3," + soma + ",'" + ForeignID + "')";
                    stm.executeUpdate(cabecalho);
                    String lastHeader = "SELECT MAX(OrderConfirmationID) AS max FROM OrderConfirmationHeader";
                    try (Statement sttm = DB_Con.getConnection().createStatement();
                                ResultSet rs = sttm.executeQuery(lastHeader)) {
                            rs.next();
                            headerid = (rs.getInt("max"));
                        }
                        catch (Exception e) {
                            e.printStackTrace();
                        }
                    //Vamos fazer um backup do XML da encomenda @Jleal
                    backupXML(fXmlFile, ForeignID);
                }
            }

            NodeList lista2 = doc.getElementsByTagName("OrderConfirmationLineItem");
            //INSERIR LINHAS
            for (int temp = 0; temp < lista2.getLength(); temp++) {

                Node nNode = lista2.item(temp);

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;

                    //System.out.println("Linha :" + Integer.valueOf(eElement.getElementsByTagName("OrderConfirmationLineItemNumber").item(0).getTextContent()));
                    int ProductIdentifierBuyer = (Integer.valueOf(eElement.getElementsByTagName("ProductIdentifier").item(0).getTextContent()));
                    int ProductIdentifierSupplier = (Integer.valueOf(eElement.getElementsByTagName("ProductIdentifier").item(1).getTextContent()));
                    int PaperLength = (Integer.valueOf(eElement.getElementsByTagName("Value").item(0).getTextContent()));
                    int PaperWidth = (Integer.valueOf(eElement.getElementsByTagName("Value").item(1).getTextContent()));
                    double GramsPerSquareMeter = (Double.valueOf(eElement.getElementsByTagName("DetailValue").item(0).getTextContent()));

                    double PricePerUnit = (Double.valueOf(eElement.getElementsByTagName("CurrencyValue").item(0).getTextContent()));
                    String UnitCurrencyType = (eElement.getElementsByTagName("CurrencyValue").item(0).getAttributes().item(0).getTextContent());
                    String UnitType = (eElement.getElementsByTagName("Value").item(2).getAttributes().item(0).getTextContent());
                    double TotalLinePrice = (Double.valueOf(eElement.getElementsByTagName("CurrencyValue").item(1).getTextContent()));
                    String TotalLineCurrency = (eElement.getElementsByTagName("CurrencyValue").item(1).getAttributes().item(0).getTextContent());

                    /*
                  System.out.println(Integer.valueOf(eElement.getElementsByTagName("MonetaryAdjustmentLine").item(0).getTextContent()));
                    System.out.println(eElement.getElementsByTagName("MonetaryAdjustment").item(0).getAttributes().item(0).getTextContent());
                     */
                    double TaxPercent = (Double.valueOf(eElement.getElementsByTagName("TaxPercent").item(0).getTextContent()));
                    String TaxCategoryType = (eElement.getElementsByTagName("TaxAdjustment").item(0).getAttributes().item(0).getTextContent());
                    String TaxType = (eElement.getElementsByTagName("TaxAdjustment").item(0).getAttributes().item(1).getTextContent());
                    double TaxLineValue = (Double.valueOf(eElement.getElementsByTagName("CurrencyValue").item(2).getTextContent()));
                    String TaxLocation = (eElement.getElementsByTagName("TaxLocation").item(0).getTextContent());

                    String QuantityType = (eElement.getElementsByTagName("Value").item(3).getAttributes().item(0).getTextContent());
                    int Quantity1 = (Integer.valueOf(eElement.getElementsByTagName("Value").item(3).getTextContent()));

                    /*
                    Validar tipo de papel para ir buscar o paperID @Leal
                     */
                    int PaperID = 0;
                    PaperID = validar.validarPaperType(PaperLength, PaperWidth);
                    /*
                    * Insere apenas a primeira quantidade registada no XML deixando as outras em NULL que serão preenchidas por um trigger
                     */
                    Statement sttm = DB_Con.getConnection().createStatement();
                    if (QuantityType.equals("Ream")) {
                        String insertLine = "INSERT INTO OrderConfirmationLine (OrderConfirmationID,ProductIdentifierBuyer, ProductIdentifierSupplier, TaxPercent, TaxLocation, TaxType, PricePerUnit, QuantityReam, TotalLinePrice, GramsPerSquareMeter, OrderStatus, PaperID) VALUES (" + headerid + "," + ProductIdentifierBuyer + " , " + ProductIdentifierSupplier + ", " + TaxPercent + ",'" + TaxLocation + "','" + TaxType + "'," + PricePerUnit + "," + Quantity1 + "," + TotalLinePrice + "," + GramsPerSquareMeter + ",0," + PaperID + ")";
                        sttm.executeUpdate(insertLine);
                        if (temp == 1) {
                            System.out.println("Encomenda inserida na Base de Dados com sucesso!");
                        }

                    } else if (QuantityType.equals("Kilogram")) {
                        String insertLine = "INSERT INTO OrderConfirmationLine (OrderConfirmationID,ProductIdentifierBuyer, ProductIdentifierSupplier, TaxPercent, TaxLocation, TaxType, PricePerUnit, QuantityKilogram, TotalLinePrice, GramsPerSquareMeter, OrderStatus, PaperID) VALUES (" + headerid + "," + ProductIdentifierBuyer + " , " + ProductIdentifierSupplier + ", " + TaxPercent + ",'" + TaxLocation + "','" + TaxType + "'," + PricePerUnit + "," + Quantity1 + "," + TotalLinePrice + "," + GramsPerSquareMeter + ",0," + PaperID + ")";
                        sttm.executeUpdate(insertLine);
                        if (temp == 1) {
                            System.out.println("Encomenda inserida na Base de Dados com sucesso!");
                        }
                    } else if (QuantityType.equals("Sheet")) {
                        String insertLine = "INSERT INTO OrderConfirmationLine (OrderConfirmationID,ProductIdentifierBuyer, ProductIdentifierSupplier, TaxPercent, TaxLocation, TaxType, PricePerUnit, QuantitySheet, TotalLinePrice, GramsPerSquareMeter, OrderStatus, PaperID) VALUES (" + headerid + "," + ProductIdentifierBuyer + " , " + ProductIdentifierSupplier + ", " + TaxPercent + ",'" + TaxLocation + "','" + TaxType + "'," + PricePerUnit + "," + Quantity1 + "," + TotalLinePrice + "," + GramsPerSquareMeter + ",0," + PaperID + ")";
                        sttm.executeUpdate(insertLine);
                        if (temp == 1) {
                            System.out.println("Encomenda inserida na Base de Dados com sucesso!");
                        }
                    }
                }
            }
        } catch (Exception e) {
            System.out.println(e);
            logsXML(e,filename);

        }
    }

    /**
     * Método para gravar cópia do ficheiro XML (guarda na pasta "BackupXML" no
     * diretorio do projeto) @JLEAL
     *
     * @param fXmlFile
     * @param no
     * @throws IOException
     */
    public static void backupXML(File fXmlFile, String no) throws IOException {

        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(fXmlFile), "UTF-8"));
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("BackupXML/bk_encomenda" + no + ".xml"), "UTF-8"));
        String line = null;
        while ((line = reader.readLine()) != null) {
            writer.write(line);
        }
        reader.close();
        writer.close();
    }

    /**
     * Método para guardar logs em caso de falha ao ler o ficheiro XML (escreve a exception e o filename num ficheiro txt) @JLEAL
     *
     * @param exception
     * @param filename
     * @throws IOException
     */
    public static void logsXML(Exception exception,String filename) throws IOException {
      Logger logger = Logger.getLogger("MyLog");  
      FileHandler fh;  
    
    try {
        fh = new FileHandler("Logs/LogsXML.txt",true);  
        logger.addHandler(fh);
        SimpleFormatter formatter = new SimpleFormatter();  
        fh.setFormatter(formatter);  

        //Escrever no ficheiro a exceção e o nome do ficheiro inserido pelo utilizador
        logger.info(exception+"  || O utilizador escreveu : "+filename);  

    } catch (SecurityException e) {  
        e.printStackTrace();  
    } catch (IOException e) {  
        e.printStackTrace();  
    } 


}
}
