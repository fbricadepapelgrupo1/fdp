package com.mycompany.fdp.storage;

import com.mycompany.fdp.database.DB_Con;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public abstract class Storage<T> {
    public abstract RowMapper<T> getRowMapper();

    public List<T> listRows(String query, Object... args) throws SQLException {
        List<T> rows = new ArrayList<T>();

        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            Connection connection = DB_Con.getConnection();
            setObjects(statement = connection.prepareStatement(query), args);
            resultSet = statement.executeQuery();

            while (resultSet.next()) {
                rows.add(getRowMapper().mapRow(resultSet));
            }
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

        return rows;
    }

    public T getRow(String query, Object... args) throws SQLException {
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            Connection connection = DB_Con.getConnection();
            setObjects(statement = connection.prepareStatement(query), args);
            resultSet = statement.executeQuery();

            if (resultSet.next()) {
                return getRowMapper().mapRow(resultSet);
            }
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

        return null;
    }

    public int updateRow(String query, Object... args) throws SQLException {
        PreparedStatement statement = null;

        try {
            Connection connection = DB_Con.getConnection();
            setObjects(statement = connection.prepareStatement(query), args);
            return statement.executeUpdate();
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void setObjects(PreparedStatement statement, Object... args) throws SQLException {
        for (int i = 1; i <= args.length; i++) {
            statement.setObject(i, args[i - 1]);
        }
    }
}