package com.mycompany.fdp.storage;

import com.mycompany.fdp.models.Stock;
import java.sql.SQLException;
/**
 *
 * @author Rafael Granja
 */
public class StockStorage extends Storage<Stock> {
     //rowmapper converte uma linha da bd para um usuario
    @Override
    public RowMapper<Stock> getRowMapper() {
        return resultSet -> new Stock(
                resultSet.getInt("StockID"),
                resultSet.getInt("PaperID")
        );
    }
    public Stock getStock(int id)throws SQLException{
        return getRow("SELECT * FROM Stock WHERE StockID=?", id);
    }
}
