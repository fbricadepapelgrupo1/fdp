/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.fdp.Model;

import com.mycompany.fdp.database.DB_Con;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Renato
 */
public class Stock {

    private static boolean getAllStock(int i) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private int stock_ID;
    private int paper_ID;
    private double grams_sqr_meter;
    private double quantity_KG;
    private int quantity_Sheet;
    private int quantity_Ream;
    private String paper_Type;
    private int length;
    private int width;

    public Stock() {
    }

    public void setStock_ID(int stock_ID) {
        this.stock_ID = stock_ID;
    }

    public void setPaper_ID(int paper_ID) {
        this.paper_ID = paper_ID;
    }

    public void setGrams_sqr_meter(double grams_sqr_meter) {
        this.grams_sqr_meter = grams_sqr_meter;
    }

    public void setQuantity_KG(double quantity_KG) {
        this.quantity_KG = quantity_KG;
    }

    public void setQuantity_Sheet(int quantity_Sheet) {
        this.quantity_Sheet = quantity_Sheet;
    }

    public void setQuantity_Ream(int quantity_Ream) {
        this.quantity_Ream = quantity_Ream;
    }

    public void setPaper_Type(String paper_Type) {
        this.paper_Type = paper_Type;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getStock_ID() {
        return stock_ID;
    }

    public int getPaper_ID() {
        return paper_ID;
    }

    public double getGrams_sqr_meter() {
        return grams_sqr_meter;
    }

    public double getQuantity_KG() {
        return quantity_KG;
    }

    public int getQuantity_Sheet() {
        return quantity_Sheet;
    }

    public int getQuantity_Ream() {
        return quantity_Ream;
    }

    public String getPaper_Type() {
        return paper_Type;
    }

    public int getLength() {
        return length;
    }

    public int getWidth() {
        return width;
    }
    
    public Stock(int stock_ID, int paper_ID, double grams_sqr_meter, double quantity_KG, int quantity_Sheet, int quantity_Ream) {
        this.stock_ID = stock_ID;
        this.paper_ID = paper_ID;
        this.grams_sqr_meter = grams_sqr_meter;
        this.quantity_KG = quantity_KG;
        this.quantity_Sheet = quantity_Sheet;
        this.quantity_Ream = quantity_Ream;
        this.paper_Type = paper_Type;
        this.length = length;
        this.width = width;
    }
    /**
     * tranforma em string 
     * @return string
     */
    @Override
    public String toString() {
        return ("Stock ID:" + this.getStock_ID()
                + " Paper ID: " + this.getPaper_ID()
                + " Grams per square meter: " + this.getGrams_sqr_meter()
                + " Quantity KG : " + this.getQuantity_KG()
                + " Quantity Sheet : " + this.getQuantity_Sheet()
                + " Quantity Ream : " + this.getQuantity_Ream());

    }
    
    /**Método que vai buscar à base de dados todo o stock atual, metendo-o num
     * array para depois ser feito o print 
     * @return
     * @throws ClassNotFoundException
     * @throws SQLException 
     */
    public static ArrayList<Stock> getAllStock() throws ClassNotFoundException, SQLException {

        DB_Con DB_Con = new DB_Con();
        DB_Con.getConnection();
        Statement stm = DB_Con.getConnection().createStatement();
        String sql = "Select * From Stock";
        ResultSet rst;
        rst = stm.executeQuery(sql);
        ArrayList<Stock> stockList = new ArrayList<>();
        while (rst.next()) {
            Stock stock = new Stock(rst.getInt("StockID"), rst.getInt("PaperID"), rst.getDouble("GramsPerSquareMeter"), rst.getDouble("QuantityKilogram"),
                    rst.getInt("QuantitySheet"), rst.getInt("QuantityReam"));
            stockList.add(stock);
        }

        return stockList;

    }
    /**
     * Método que imprime todos os itens do array de stock
     * @throws SQLException
     * @throws ClassNotFoundException 
     */
    public void listarStock() throws SQLException, ClassNotFoundException {
        int i = 0;
        while (i < Stock.getAllStock().size()) {
            System.out.println(Stock.getAllStock().get(i));
            i++;
        }
        System.out.println("Stock printed - caso nada seja apresentado, nao existe stock");
    }
}
