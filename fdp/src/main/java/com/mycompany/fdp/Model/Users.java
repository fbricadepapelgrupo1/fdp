/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.fdp.Model;

import com.mycompany.fdp.Database.DB_Con;
import com.mycompany.fdp.Login;
import com.mycompany.fdp.MainApp;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Scanner;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import validations.ValidationsUsers;

/**
 *
 * @author Renato
 */
public class Users {

    public boolean uservalido = false;
    public boolean campovazio = false;
    public boolean campoletras = false;
    public boolean camponumero = false;
    public static ValidationsUsers validar = new ValidationsUsers();
    Scanner scanner = new Scanner(System.in);
    private int user_ID;
    private String name;
    private String address1;
    private String address2;
    private String city;
    private String postal_Code;
    private String Country;
    private String user_Name;
    private String password;
    private int user_Type;

    public void setUser_ID(int user_ID) {
        this.user_ID = user_ID;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setPostal_Code(String postal_Code) {
        this.postal_Code = postal_Code;
    }

    public void setCountry(String Country) {
        this.Country = Country;
    }

    public void setUser_Name(String user_Name) {
        this.user_Name = user_Name;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setUser_Type(int user_Type) {
        this.user_Type = user_Type;
    }

    public int getUser_ID() {
        return user_ID;
    }

    public String getName() {
        return name;
    }

    public String getAddress1() {
        return address1;
    }

    public String getAddress2() {
        return address2;
    }

    public String getCity() {
        return city;
    }

    public String getPostal_Code() {
        return postal_Code;
    }

    public String getCountry() {
        return Country;
    }

    public String getUser_Name() {
        return user_Name;
    }

    public String getPassword() {
        return password;
    }

    public int getUser_Type() {
        return user_Type;
    }

    public Users() {
    }

    public Users(int user_ID, String name, String address1, String address2, String city, String postal_Code, String Country, String user_Name, String password, int user_Type) {
        this.user_ID = user_ID;
        this.name = name;
        this.address1 = address1;
        this.address2 = address2;
        this.city = city;
        this.postal_Code = postal_Code;
        this.Country = Country;
        this.user_Name = user_Name;
        this.password = password;
        this.user_Type = user_Type;
    }

    public void adminCriarUtilizador() throws SQLException, ClassNotFoundException, SAXException, ParserConfigurationException, IOException {
//Coneçao a Base de dados e validações de input
        DB_Con DB_Con = new DB_Con();
        Statement query1 = DB_Con.createConnection().createStatement();
        ResultSet rs1 = query1.executeQuery("SELECT UserName FROM Users");

        System.out.println("Escolha o tipo de utilizador");
        if (scanner.hasNextInt()) {
            user_Type = scanner.nextInt();
        } else {
            scanner.next();
            user_Type = 3;
        }

        while (user_Type != 0) {
            System.out.print("Numero do fornecedor so pode ser 0");
            if (scanner.hasNextInt()) {
                user_Type = scanner.nextInt();
            } else {
                String invalido = scanner.next();
                user_Type = 4;
            }
        }

       

        do {

            System.out.println("Introduza o Username");
            user_Name = scanner.nextLine();
            if (!validar.validarExisteUsername(user_Name)) {
                uservalido = true;
            }
        } while (!uservalido);

        /*while (rs1.next()) {
            if (rs1.getString("UserName").equals(user_Name)) {
                System.out.println("O username ja existe, por favor reescreva o Username");
                user_Name = scanner.nextLine();
            } else {
                break;
            }
        }
         */
        do {

            user_Name = scanner.nextLine();
            if (!validar.validarCampoVazioString(user_Name)) {
                campovazio = true;
            }
        } while (!campovazio);

        do {
            System.out.println("Introduza a password");
            password = scanner.nextLine();
            if (!validar.validarCampoVazioString(password)) {
                campovazio = true;
            }
        } while (!campovazio);

        do {
            System.out.println("Introduza o nome");
            name = scanner.nextLine();
            if (!validar.validarCampoVazioString(name)) {
                campovazio = true;
            }
        } while (!campovazio);

        do {
            name = scanner.nextLine();
            if (!validar.validarCampoLetras(name)) {
                campoletras = true;
            }
        } while (!campoletras);

        do {
            System.out.println("Introduza a morada");
            address1 = scanner.nextLine();
            if (!validar.validarCampoVazioString(address1)) {
                campovazio = true;
            }
        } while (!campovazio);

        do {
            address1 = scanner.nextLine();
            if (!validar.validarCampoLetras(address1)) {
                campoletras = true;
            }
        } while (!campoletras);

        do {
            System.out.println("Introduza o numero de porta/lote");
            address2 = scanner.nextLine();
            if (!validar.validarCampoVazioString(address2)) {
                campovazio = true;
            }
        } while (!campovazio);

        do {

            address2 = scanner.nextLine();
            if (!validar.validarCampoNumeros(address2)) {
                camponumero = true;
            }
        } while (!camponumero);

        do {
            System.out.println("Introduza a cidade");
            city = scanner.nextLine();
            if (!validar.validarCampoVazioString(city)) {
                campovazio = true;
            }
        } while (!campovazio);

        do {
            city = scanner.nextLine();
            if (!validar.validarCampoLetras(city)) {
                campoletras = true;
            }
        } while (!campoletras);

        do {
            System.out.println("Introduza o codigo postal");
            postal_Code = scanner.next();
            if (!validar.validarCampoLetras(postal_Code)) {
                campovazio = true;
            }
        } while (!campovazio);

        do {
            postal_Code = scanner.next();
            if (!validar.validarCampoNumeros(postal_Code)) {
                camponumero = true;
            }
        } while (!camponumero);

        do {
            System.out.println("Introduza o pais");
            Country = scanner.next();

            if (!validar.validarCampoVazioString(Country)) {
                campovazio = true;
            }
        } while (!campovazio);

        do {
            Country = scanner.next();

            if (!validar.validarCampoLetras(Country)) {
                campoletras = true;
            }
        } while (!campoletras);

        /*
        Menu de confirmação, que apresenta todas as variáveis introduzidas,
        com a opção de cancelar (tecla c) ou introduzir na base de dados (tecla y).
         */
        System.out.println("Confirmar operacao, pressionar c para cancelar e y para confirmar");
        System.out.printf("user_Type: %s\nUsername: %s\nPassword: %s\nName: %s\nAddress1: %s\nAddress2: %s\nCity: %s\nPostal_Code: %s\nCountry: %s\n", user_Type, user_Name, password, name, address1, address2, city, postal_Code, Country);

        while (true) {
            String input;
            input = scanner.nextLine();
            if (input.equals("c")) {
                System.out.println("Operacao cancelada, a voltar para o menu");
                MainApp.main(null);
            } else if (input.equals("y")) {
                try {

                    String query = "INSERT INTO Users(UserType,UserName,UserPassword,UName,Address1,Address2,City,PostalCode,Country) "
                            + "VALUES ('" + user_Type + "','" + user_Name + "','" + password + "','" + name + "','" + address1 + "','" + address2 + "','" + city + "','" + postal_Code + "','" + Country + "')";

                    Statement queryBD = DB_Con.createConnection().createStatement();
                    queryBD.executeUpdate(query);

                    System.out.println("Base de dados atualizada");
                } catch (SQLException exception) {
                    System.out.println(exception.getMessage());
                }
            }
        }
    }

    public void operadorCriarFornecedor() throws SQLException, ClassNotFoundException, SAXException, ParserConfigurationException, IOException {
//Coneçao a Base de dados e validações de input
        DB_Con DB_Con = new DB_Con();
        Statement query1 = DB_Con.createConnection().createStatement();
        ResultSet rs1 = query1.executeQuery("SELECT UserName FROM Users");

        System.out.println("Escolha o tipo de utilizador");
        if (scanner.hasNextInt()) {
            user_Type = scanner.nextInt();
        } else {
            scanner.next();
            user_Type = 3;
        }

        while (user_Type != 0) {
            System.out.print("Numero do fornecedor so pode ser 0");
            if (scanner.hasNextInt()) {
                user_Type = scanner.nextInt();
            } else {
                String invalido = scanner.next();
                user_Type = 4;
            }
        }

        scanner.nextLine();

        do {

            System.out.println("Introduza o Username");
            user_Name = scanner.nextLine();
            if (!validar.validarExisteUsername(user_Name)) {
                uservalido = true;
            }
        } while (!uservalido);

        /*while (rs1.next()) {
            if (rs1.getString("UserName").equals(user_Name)) {
                System.out.println("O username ja existe, por favor reescreva o Username");
                user_Name = scanner.nextLine();
            } else {
                break;
            }
        }
         */
        do {

            user_Name = scanner.nextLine();
            if (!validar.validarCampoVazioString(user_Name)) {
                campovazio = true;
            }
        } while (!campovazio);

        do {
            System.out.println("Introduza a password");
            password = scanner.nextLine();
            if (!validar.validarCampoVazioString(password)) {
                campovazio = true;
            }
        } while (!campovazio);

        do {
            System.out.println("Introduza o nome");
            name = scanner.nextLine();
            if (!validar.validarCampoVazioString(name)) {
                campovazio = true;
            }
        } while (!campovazio);

        do {
            name = scanner.nextLine();
            if (!validar.validarCampoLetras(name)) {
                campoletras = true;
            }
        } while (!campoletras);

        do {
            System.out.println("Introduza a morada");
            address1 = scanner.nextLine();
            if (!validar.validarCampoVazioString(address1)) {
                campovazio = true;
            }
        } while (!campovazio);

        do {
            address1 = scanner.nextLine();
            if (!validar.validarCampoLetras(address1)) {
                campoletras = true;
            }
        } while (!campoletras);

        do {
            System.out.println("Introduza o numero de porta/lote");
            address2 = scanner.nextLine();
            if (!validar.validarCampoVazioString(address2)) {
                campovazio = true;
            }
        } while (!campovazio);

        do {

            address2 = scanner.nextLine();
            if (!validar.validarCampoNumeros(address2)) {
                camponumero = true;
            }
        } while (!camponumero);

        do {
            System.out.println("Introduza a cidade");
            city = scanner.nextLine();
            if (!validar.validarCampoVazioString(city)) {
                campovazio = true;
            }
        } while (!campovazio);

        do {
            city = scanner.nextLine();
            if (!validar.validarCampoLetras(city)) {
                campoletras = true;
            }
        } while (!campoletras);

        do {
            System.out.println("Introduza o codigo postal");
            postal_Code = scanner.next();
            if (!validar.validarCampoLetras(postal_Code)) {
                campovazio = true;
            }
        } while (!campovazio);

        do {
            postal_Code = scanner.next();
            if (!validar.validarCampoNumeros(postal_Code)) {
                camponumero = true;
            }
        } while (!camponumero);

        do {
            System.out.println("Introduza o pais");
            Country = scanner.next();

            if (!validar.validarCampoVazioString(Country)) {
                campovazio = true;
            }
        } while (!campovazio);

        do {
            Country = scanner.next();

            if (!validar.validarCampoLetras(Country)) {
                campoletras = true;
            }
        } while (!campoletras);

        /*
        Menu de confirmação, que apresenta todas as variáveis introduzidas,
        com a opção de cancelar (tecla c) ou introduzir na base de dados (tecla y).
         */
        System.out.println("Confirmar operacao, pressionar c para cancelar e y para confirmar");
        System.out.printf("user_Type: %s\nUsername: %s\nPassword: %s\nName: %s\nAddress1: %s\nAddress2: %s\nCity: %s\nPostal_Code: %s\nCountry: %s\n", user_Type, user_Name, password, name, address1, address2, city, postal_Code, Country);

        while (true) {
            String input;
            input = scanner.nextLine();
            if (input.equals("c")) {
                System.out.println("Operacao cancelada, a voltar para o menu");
                MainApp.main(null);
            } else if (input.equals("y")) {
                try {

                    String query = "INSERT INTO Users(UserType,UserName,UserPassword,UName,Address1,Address2,City,PostalCode,Country) "
                            + "VALUES ('" + user_Type + "','" + user_Name + "','" + password + "','" + name + "','" + address1 + "','" + address2 + "','" + city + "','" + postal_Code + "','" + Country + "')";

                    Statement queryBD = DB_Con.createConnection().createStatement();
                    queryBD.executeUpdate(query);

                    System.out.println("Base de dados atualizada");
                } catch (SQLException exception) {
                    System.out.println(exception.getMessage());
                }
            }
        }
    }

    @Override
    public String toString() {
        return ("ID:" + this.getUser_ID()
                + " Nome: " + this.getName()
                + " Morada 1: " + this.getAddress1()
                + " Morada 2 : " + this.getAddress2()
                + " Cidade : " + this.getCity()
                + " Código Postal : " + this.getPostal_Code()
                + " País : " + this.getCountry()
                + " Username : " + this.getUser_Name()
                + " Tipo de Utilizador : " + this.getUser_Type());
    }

    public static void apagarUtilizador() throws ClassNotFoundException, SQLException, SAXException, ParserConfigurationException, IOException {

        Scanner scanner = new Scanner(System.in);
        int idUser = 0;

        System.out.println("-----------------------");
        System.out.println("   Apagar Utilizador   ");
        System.out.println("-----------------------");
        System.out.println("");
        System.out.println("Utilizadores:");

        //Listagem de Utilizadores
        getUsers();

        //Pedir Id de User
        System.out.println("Insira o ID do Utilizador que pretende remover.");
        System.out.println("ID: ");
        idUser = scanner.nextInt();

        Statement stm2 = DB_Con.createConnection().createStatement();
        String sql2 = "SELECT * FROM Users WHERE UserID = '" + idUser + "'";
        ResultSet rst2;
        rst2 = stm2.executeQuery(sql2);

        while (rst2.next()) {

            System.out.println("------");
            System.out.println("Dados de Utilizador");
            System.out.println("------");
            System.out.println("Id: " + rst2.getInt("UserID"));

            System.out.println("Nome: " + rst2.getString("UName"));
            System.out.println("Username: " + rst2.getInt("UserType"));
            //Tipo de Utilizador em Texto
            String typeUser = "";

            if (rst2.getInt("UserType") == 0) {
                typeUser = "Fornecedor";
            } else if (rst2.getInt("UserType") == 1) {
                typeUser = "Operador";
            } else if (rst2.getInt("UserType") == 2) {
                typeUser = "Administrador";
            } else {
                typeUser = "Desconhecido";
            }

            System.out.println("Tipo de Utilizador: " + typeUser);
            System.out.println("");
        }

        System.out.println("Confirmação de Operação. Pressione C para cancelar e Y para confirmar.");

        while (true) {
            String input;
            input = scanner.nextLine();

            if (input.equals("c") || input.equals("C")) {

                System.out.println("Operacao cancelada, a voltar para o menu");
                MainApp.main(null);

            } else if (input.equals("y") || input.equals("Y")) {

                try {

                    String query = "DELETE FROM Users WHERE UserID = '" + idUser + "'";
                    Statement queryBD = DB_Con.createConnection().createStatement();
                    queryBD.executeUpdate(query);
                    System.out.println("O utilizador com o id " + idUser + " foi apagado com sucesso.");

                } catch (SQLException exception) {

                    //System.out.println(exception.getMessage());
                }
            }
        }
    }

    public static ArrayList<Users> getUsers() throws ClassNotFoundException, SQLException {

        Statement stm = DB_Con.createConnection().createStatement();
        String sql = "Select * From Users";
        ResultSet rst;
        rst = stm.executeQuery(sql);

        ArrayList<Users> usersList = new ArrayList<>();

        while (rst.next()) {
            Users users = new Users(rst.getInt("UserID"), rst.getString("UName"),
                    rst.getString("Address1"), rst.getString("Address2"),
                    rst.getString("City"), rst.getString("PostalCode"),
                    rst.getString("Country"), rst.getString("UserPassword"),
                    rst.getString("UserName"), rst.getInt("UserType"));
            usersList.add(users);
        }
        System.out.println("");
        System.out.println("-----------------------");
        for (int i = 0; i < usersList.size(); i++) {
            System.out.println(usersList.get(i));
        }
        System.out.println("-----------------------");
        System.out.println("");

        return usersList;
    }

    public static void EditarUsers() throws SQLException, ClassNotFoundException, IOException, ParserConfigurationException, ClassNotFoundException, SAXException {
        Scanner ler = new Scanner(System.in);
        System.out.println("Insira o ID do user que pretende editar: ");
        String userid = ler.nextLine();
        String update;

        Statement stm = DB_Con.createConnection().createStatement();

        char opcao;
        do {
            System.out.println("Menu");
            System.out.println(" 1-Nome");
            System.out.println(" 2-Rua");
            System.out.println(" 3-Número da Porta");
            System.out.println(" 4-Cidade");
            System.out.println(" 5-Código Postal");
            System.out.println(" 6-País");
            System.out.println(" 7-Password");
            System.out.println(" 8-Utilizador");
            System.out.println(" 9-Sair");
            System.out.println("Escolha uma opção:");
            opcao = ler.next().charAt(0);
            String newName = "";
            String newRua = "";
            String newNumero = "";
            String newCidade = "";
            String newCodigo = "";
            String newPais = "";
            String newPW = "";
            String newUser = "";

            switch (opcao) {
                case '1':
                    System.out.println("Insira o nome do utilizador:");
                    while (newName.trim().isEmpty()) {
                        newName = ler.nextLine();
                    }
                    try {
                        update = "UPDATE Users SET UName= '" + newName + "' WHERE UserID='" + userid + "'";
                        stm.executeUpdate(update);
                        System.out.println("Nome do fornecedor atualizado!");
                    } catch (Exception e) {
                        System.out.print(e);
                    }
                    break;
                case '2':
                    System.out.println("Insira a nova Rua:");
                    while (newRua.isEmpty()) {
                        newName = ler.nextLine();
                    }
                    try {
                        update = "UPDATE Users SET Address1= '" + newRua + "' WHERE UserID='" + userid + "'";
                        stm.executeUpdate(update);
                        System.out.println("Rua atualizada!");
                    } catch (Exception e) {
                        System.out.print(e);
                    }
                case '3':
                    System.out.println("Insira o novo número da Porta:");
                    while (newRua.isEmpty()) {
                        newName = ler.nextLine();
                    }
                    try {
                        update = "UPDATE Users SET Address2= '" + newNumero + "' WHERE UserID='" + userid + "'";
                        stm.executeUpdate(update);
                        System.out.println("Número da Porta atualizado!");
                    } catch (Exception e) {
                        System.out.print(e);
                    }
                case '4':
                    System.out.println("Insira a nova cidade:");
                    while (newRua.isEmpty()) {
                        newName = ler.nextLine();
                    }
                    try {
                        update = "UPDATE Users SET City= '" + newCidade + "' WHERE UserID='" + userid + "'";
                        stm.executeUpdate(update);
                        System.out.println("Cidade atualizada!");
                    } catch (Exception e) {
                        System.out.print(e);
                    }
                case '5':
                    System.out.println("Insira o novo código postal:");
                    while (newRua.isEmpty()) {
                        newName = ler.nextLine();
                    }
                    try {
                        update = "UPDATE Users SET PostalCode= '" + newCodigo + "' WHERE UserID='" + userid + "'";
                        stm.executeUpdate(update);
                        System.out.println("Código Postal atualizado!");
                    } catch (Exception e) {
                        System.out.print(e);
                    }
                case '6':
                    System.out.println("Insira o novo País:");
                    while (newRua.isEmpty()) {
                        newName = ler.nextLine();
                    }
                    try {
                        update = "UPDATE Users SET Country= '" + newPais + "' WHERE UserID='" + userid + "'";
                        stm.executeUpdate(update);
                        System.out.println("País atualizado!");
                    } catch (Exception e) {
                        System.out.print(e);
                    }
                case '7':
                    System.out.println("Insira a nova password:");
                    while (newRua.isEmpty()) {
                        newName = ler.nextLine();
                    }
                    try {
                        update = "UPDATE Users SET UserPassword= '" + newPW + "' WHERE UserID='" + userid + "'";
                        stm.executeUpdate(update);
                        System.out.println("Password atualizada!");
                    } catch (Exception e) {
                        System.out.print(e);
                    }
                case '8':
                    System.out.println("Insira o novo nome de utilizador:");
                    while (newRua.isEmpty()) {
                        newName = ler.nextLine();
                    }
                    try {
                        update = "UPDATE Users SET UserName= '" + newUser + "' WHERE UserID='" + userid + "'";
                        stm.executeUpdate(update);
                        System.out.println("Nome de utilizador atualizado!");
                    } catch (Exception e) {
                        System.out.print(e);
                    }
                default:

                    System.out.println("A voltar para o login");
                    Login log = new Login();
                    log.login1();
            }
        } while (opcao != '9');
        {

        }
    }

}
