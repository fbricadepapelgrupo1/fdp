/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.fdp.Model;

import validations.xmlOrderValidation;
import com.mycompany.fdp.database.DB_Con;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author rui
 */
public class OrderConfirmationHeader {

    private int OrderConfirmationID;
    private Date OrderDate;
    private int UserID;
    private BigDecimal TotalOrderPrice;
    private String ForeignID;
    private ArrayList<OrderConfirmationLine> orderConfirmationLines;
    private static boolean lflag = false;

    public OrderConfirmationHeader() {
    }

    public OrderConfirmationHeader(int OrderConfirmationID, Date OrderDate, int UserID, BigDecimal TotalOrderPrice, String ForeignID, ArrayList<OrderConfirmationLine> orderConfirmationLines) {
        this.OrderConfirmationID = OrderConfirmationID;
        this.OrderDate = OrderDate;
        this.UserID = UserID;
        this.TotalOrderPrice = TotalOrderPrice;
        this.ForeignID = ForeignID;
        this.orderConfirmationLines = orderConfirmationLines;
    }

    public String getForeignID() {
        return ForeignID;
    }

    public Date getOrderDate() {
        return OrderDate;
    }

    public int getOrderConfirmationID() {
        return OrderConfirmationID;
    }

    public BigDecimal getTotalOrderPrice() {
        return TotalOrderPrice;
    }

    public int getUserID() {
        return UserID;
    }

    public ArrayList<OrderConfirmationLine> getOrderConfirmationLines() {
        return orderConfirmationLines;
    }

    public void setOrderConfirmationLines(ArrayList<OrderConfirmationLine> orderConfirmationLines) {
        this.orderConfirmationLines = orderConfirmationLines;
    }

    public void setForeignID(String ForeignID) {
        this.ForeignID = ForeignID;
    }

    public void setOrderDate(Date OrderDate) {
        this.OrderDate = OrderDate;
    }

    public void setOrderConfirmationID(int OrderConfirmationID) {
        this.OrderConfirmationID = OrderConfirmationID;
    }

    public void setTotalOrderPrice(BigDecimal TotalOrderPrice) {
        this.TotalOrderPrice = TotalOrderPrice;
    }

    public void setUserID(int UserID) {
        this.UserID = UserID;
    }

    /**
     * vai buscar a fatura mais antiga e prenche a class header com os dados
     * corespondetes.
     */
    public void SConfirmation() {
        try {
            DB_Con DB_Con = new DB_Con();
            DB_Con.getConnection();
            String query = "SELECT top 1 OrderConfirmationHeader.* FROM OrderConfirmationHeader inner join OrderConfirmationLine on OrderConfirmationHeader.OrderConfirmationID = OrderConfirmationLine.OrderConfirmationID WHERE OrderStatus = 0 order by OrderDate desc";
            // create the java statement
            Statement st = DB_Con.getConnection().createStatement();

            // execute the query, and get a java resultset
            ResultSet rs = st.executeQuery(query);
            if (rs.next() == false) {
                lflag = true;
            } else {
                do {
                    this.setOrderDate(rs.getDate("OrderDate"));
                    this.setForeignID(rs.getString("OrderForeignID"));
                    this.setOrderConfirmationLines(Slines(rs.getInt("OrderConfirmationID")));
                    this.setOrderConfirmationID(rs.getInt("OrderConfirmationID"));
                    this.setTotalOrderPrice(rs.getBigDecimal("TotalOrderPrice"));
                    this.setUserID(rs.getInt("UserID"));
                    st.close();
                } while (rs.next());
            }
        } catch (Exception e) {
            System.err.println("Got an exception On SConfirmation! " + e);
            System.err.println(e.getMessage());
        }
    }

    /**
     * recebe o id da metodo SConfirmation e prenche a class
     * OrderConfirmationLine a qual e retornada para o a metodo SConfirmation
     *
     * @param id
     * @return array de linhas correspondetes ao header
     */
    public static ArrayList<OrderConfirmationLine> Slines(int id) {
        ArrayList<OrderConfirmationLine> list = new ArrayList();
        try {
            DB_Con DB_Con = new DB_Con();
            DB_Con.getConnection();
            String query = "SELECT OrderConfirmationLine.* from OrderConfirmationLine WHERE OrderConfirmationID = " + id;
            // create the java statement
            Statement st = DB_Con.getConnection().createStatement();
            // execute the query, and get a java resultset
            ResultSet rs = st.executeQuery(query);
            while (rs.next()) {
                OrderConfirmationLine l = new OrderConfirmationLine();
                l.setGramsPerSquareMeter(rs.getInt("GramsPerSquareMeter"));
                l.setOrderCOnfermationID(rs.getInt("OrderConfirmationID"));
                l.setOrderConfermationLineID(rs.getInt("OrderConfimationLineID"));
                l.setOrderStatus(rs.getInt("OrderStatus"));
                l.setPaperID(rs.getInt("PaperID"));
                l.setPricePerUnit(rs.getBigDecimal("PricePerUnit"));
                l.setProductIdentifierBuyer(rs.getInt("ProductIdentifierBuyer"));
                l.setProductIdentifierSupplier(rs.getInt("ProductIdentifierSupplier"));
                l.setQuantityKilogram(rs.getInt("QuantityKilogram"));
                l.setQuantityReam(rs.getInt("QuantityReam"));
                l.setQuantitySheet(rs.getInt("QuantitySheet"));
                l.setTaxLocal(rs.getString("TaxLocation"));
                l.setTaxPercent(rs.getInt("TaxPercent"));
                l.setProductIdentifierSupplier(rs.getInt("ProductIdentifierSupplier"));
                l.setTotalLinePrice(rs.getBigDecimal("TotalLinePrice"));
                list.add(l);
            }
            st.close();
        } catch (SQLException e) {
            System.err.println("Got an exception on Slines! " + e);
            System.err.println(e.getMessage());
        }
        return list;
    }

    /**
     * class receve o id do header e atualiza todas as linhas correnspondetes ao
     * mesmo com o valor selecionado na metodo validarXML
     *
     * @param status
     * @param id
     */
    public static void Ulins(int status, int id) {
        try {
            DB_Con DB_Con = new DB_Con();
            PreparedStatement ps = DB_Con.getConnection().prepareStatement("UPDATE OrderConfirmationLine SET OrderStatus = " + status + " WHERE OrderConfirmationID = " + id);
            ps.executeUpdate();
            ps.close();
            System.out.println("Executado com sucesso");
        } catch (Exception e) {
            System.err.println("Got an exception onUlins! " + e);
            System.err.println(e.getMessage());
        }

    }

    /**
     * invoca os metodo e apresentasa ao utilizador a fatuar, em seguila pede o
     * valor para o qual vai ser incerido no metodo Ulines.
     *
     * @throws IOException
     */
    public static void validarXML() throws IOException {
        Scanner scanner = new Scanner(System.in);
        OrderConfirmationHeader Order = new OrderConfirmationHeader();
        xmlOrderValidation val = new xmlOrderValidation();
        Order.SConfirmation();
        while (!lflag) {
            ArrayList<OrderConfirmationLine> lines = Order.getOrderConfirmationLines();
            String leftAlignFormat = "| %-15d | %-15d | %-15d | %-15d | %-15s | %-15.2f | %-15d | %-15d | %-15d | %-15d | %-14d  | %-15.2f |%n";

            System.out.println("Id: " + Order.getOrderConfirmationID() + " Id Externo: " + Order.getForeignID() + " Data: " + Order.getOrderDate());
            System.out.println("Vendedor: " + Order.getUserID());
            System.out.format("+-----------------+-----------------+-----------------+-----------------+-----------------+-----------------+-----------------+-----------------+-----------------+-----------------+-----------------+-----------------+%n");
            System.out.format("| Id de linha     | Id interno      | Id externo      |  taxa           | Pais de taxa    | Preço unitario  |Quantidade Resmas|Quantidade folhas| Qunatidade kilos| Gramagem        | Id de papel     | Preço           |%n");
            System.out.format("+-----------------+-----------------+-----------------+-----------------+-----------------+-----------------+-----------------+-----------------+-----------------+-----------------+-----------------+-----------------+%n");
            for (int i = 0; i < lines.size(); i++) {
                System.out.format(leftAlignFormat, lines.get(i).getOrderConfermationLineID(), lines.get(i).getProductIdentifierBuyer(), lines.get(i).getProductIdentifierSupplier(), lines.get(i).getTaxPercent(), lines.get(i).getTaxLocal(), lines.get(i).getPricePerUnit(), lines.get(i).getQuantityReam(), lines.get(i).getQuantitySheet(), lines.get(i).getQuantityKilogram(), lines.get(i).getGramsPerSquareMeter(), lines.get(i).getPaperID(), lines.get(i).getTotalLinePrice());
            }
            System.out.format("+-----------------+-----------------+-----------------+-----------------+-----------------+-----------------+-----------------+-----------------+-----------------+-----------------+-----------------+-----------------+%n");
            System.out.println("Preço Total: " + Order.getTotalOrderPrice());
            try {
                int re;
                do {
                    System.out.println("Y para validar, N para recusar");
                    re = val.validar(Character.toUpperCase(scanner.next().charAt(0)));
                } while (re == 0);
                System.out.println(re);
                Ulins(re, Order.getOrderConfirmationID());
                Order.SConfirmation();
            } catch (Exception e) {
                System.out.println("Got an exception validarXML! " + e);
            }
        }
        System.out.println("Ther are no more results");
    }
}
