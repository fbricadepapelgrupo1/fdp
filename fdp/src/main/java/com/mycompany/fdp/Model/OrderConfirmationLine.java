/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.fdp.Model;

import java.math.BigDecimal;

/**
 *
 * @author rui
 */
public class OrderConfirmationLine {

    private int OrderConfermationLineID;
    private int OrderCOnfermationID;
    private int ProductIdentifierBuyer;
    private int ProductIdentifierSupplier;
    private int TaxPercent;
    private String TaxType;
    private String TaxLocal;
    private BigDecimal PricePerUnit;
    private int QuantitySheet;
    private int QuantityReam;
    private int QuantityKilogram;
    private BigDecimal TotalLinePrice;
    private int GramsPerSquareMeter;
    private int OrderStatus;
    private int PaperID;

    public OrderConfirmationLine(int OrderConfermationLineID, int OrderCOnfermationID, int ProductIdentifierBuyer, int ProductIdentifierSupplier, int TaxPercent, String TaxType, String TaxLocal, BigDecimal PricePerUnit, int QuantitySheet, int QuantityReam, int QuantityKilogram, BigDecimal TotalLinePrice, int GramsPerSquareMeter, int OrderStatus, int PaperID) {
        this.OrderConfermationLineID = OrderConfermationLineID;
        this.OrderCOnfermationID = OrderCOnfermationID;
        this.ProductIdentifierBuyer = ProductIdentifierBuyer;
        this.ProductIdentifierSupplier = ProductIdentifierSupplier;
        this.TaxPercent = TaxPercent;
        this.TaxType = TaxType;
        this.TaxLocal = TaxLocal;
        this.PricePerUnit = PricePerUnit;
        this.QuantitySheet = QuantitySheet;
        this.QuantityReam = QuantityReam;
        this.QuantityKilogram = QuantityKilogram;
        this.TotalLinePrice = TotalLinePrice;
        this.GramsPerSquareMeter = GramsPerSquareMeter;
        this.OrderStatus = OrderStatus;
        this.PaperID = PaperID;
    }

    public OrderConfirmationLine() {
    }

    public void setGramsPerSquareMeter(int GramsPerSquareMeter) {
        this.GramsPerSquareMeter = GramsPerSquareMeter;
    }

    public void setOrderCOnfermationID(int OrderCOnfermationID) {
        this.OrderCOnfermationID = OrderCOnfermationID;
    }

    public void setOrderConfermationLineID(int OrderConfermationLineID) {
        this.OrderConfermationLineID = OrderConfermationLineID;
    }

    public void setOrderStatus(int OrderStatus) {
        this.OrderStatus = OrderStatus;
    }

    public void setPaperID(int PaperID) {
        this.PaperID = PaperID;
    }

    public void setPricePerUnit(BigDecimal PricePerUnit) {
        this.PricePerUnit = PricePerUnit;
    }

    public void setProductIdentifierBuyer(int ProductIdentifierBuyer) {
        this.ProductIdentifierBuyer = ProductIdentifierBuyer;
    }

    public void setQuantityKilogram(int QuantityKilogram) {
        this.QuantityKilogram = QuantityKilogram;
    }

    public void setQuantityReam(int QuantityReam) {
        this.QuantityReam = QuantityReam;
    }

    public void setQuantitySheet(int QuantitySheet) {
        this.QuantitySheet = QuantitySheet;
    }

    public void setTaxLocal(String TaxLocal) {
        this.TaxLocal = TaxLocal;
    }

    public void setTaxPercent(int TaxPercent) {
        this.TaxPercent = TaxPercent;
    }

    public void setTaxType(String TaxType) {
        this.TaxType = TaxType;
    }


    public void setTotalLinePrice(BigDecimal TotalLinePrice) {
        this.TotalLinePrice = TotalLinePrice;
    }

    public void setProductIdentifierSupplier(int ProductIdentifierSupplier) {
        this.ProductIdentifierSupplier = ProductIdentifierSupplier;
    }

    public int getProductIdentifierSupplier() {
        return ProductIdentifierSupplier;
    }

    public int getGramsPerSquareMeter() {
        return GramsPerSquareMeter;
    }

    public int getOrderCOnfermationID() {
        return OrderCOnfermationID;
    }

    public int getOrderConfermationLineID() {
        return OrderConfermationLineID;
    }

    public int getOrderStatus() {
        return OrderStatus;
    }

    public int getPaperID() {
        return PaperID;
    }

    public BigDecimal getPricePerUnit() {
        return PricePerUnit;
    }

    public int getProductIdentifierBuyer() {
        return ProductIdentifierBuyer;
    }

    public int getQuantityKilogram() {
        return QuantityKilogram;
    }

    public int getQuantityReam() {
        return QuantityReam;
    }

    public int getQuantitySheet() {
        return QuantitySheet;
    }

    public String getTaxLocal() {
        return TaxLocal;
    }

    public int getTaxPercent() {
        return TaxPercent;
    }

    public String getTaxType() {
        return TaxType;
    }

    public BigDecimal getTotalLinePrice() {
        return TotalLinePrice;
    }
   
}
