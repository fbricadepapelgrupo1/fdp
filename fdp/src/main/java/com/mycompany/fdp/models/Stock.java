
package com.mycompany.fdp.models;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author Rafael Granja
 */
public class Stock {
    private final int id;
    private int paperId;
    
    @JsonCreator
    public Stock(
        @JsonProperty("id") int id,
        @JsonProperty("paperId") int paperId
    ){
        this.id = id;
        this.paperId = paperId;      
}
    public int getId() {
        return id;
    }
    public void setPaperId(int paperId) {
        this.paperId = paperId;
    }

    public int getPaperId() {
        return paperId;
    }
    
}
