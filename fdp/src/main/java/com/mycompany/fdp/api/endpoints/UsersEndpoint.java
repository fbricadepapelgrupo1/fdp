package com.mycompany.fdp.api.endpoints;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mycompany.fdp.api.Endpoint;
import com.mycompany.fdp.models.ApiResponse;
import com.mycompany.fdp.models.User;
import com.mycompany.fdp.storage.UserStorage;

import java.sql.SQLException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UsersEndpoint extends Endpoint {
    private final UserStorage storage = new UserStorage();
    private final Pattern idPattern = Pattern.compile("^(?<id>[0-9]+)/$");

    private final ObjectMapper objectMapper;
    public UsersEndpoint(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Override
    public ApiResponse get(String path) throws SQLException {
        if (path.isEmpty()) {
            return ApiResponse.success(200, storage.getAllUsers());
        }

        Matcher idMatch = idPattern.matcher(path);
        if (idMatch.matches()) {
            int id = Integer.parseInt(idMatch.group("id"));
            User user = storage.getUser(id);
            if (user != null) {
                return ApiResponse.success(200, user);
            } else return ApiResponse.error(404, "There is no such user with id: " + id);
        }

        return super.get(path);
    }

    @Override
    public ApiResponse post(String path, String payload) throws SQLException {
        if (path.isEmpty()) {
            try {
                User parsedUser = objectMapper.readValue(payload, User.class);
                User createdUser = storage.createUser(parsedUser);
                return ApiResponse.success(201, createdUser);
            } catch (JsonProcessingException e) {
                return ApiResponse.error(400, e.getOriginalMessage());
            }
        }

        return super.post(path, payload);
    }

    @Override
    public ApiResponse put(String path, String payload) throws SQLException {
        Matcher idMatch = idPattern.matcher(path);
        if (idMatch.matches()) {
            int id = Integer.parseInt(idMatch.group("id"));
            User user = storage.getUser(id);
            if (user != null) {
                try {
                    User parsedUser = objectMapper.readValue(payload, User.class);
                    storage.updateUser(user, parsedUser);
                    return ApiResponse.success(200, parsedUser);
                } catch (JsonProcessingException e) {
                    return ApiResponse.error(400, e.getOriginalMessage());
                }
            } else return ApiResponse.error(404, "There is no such user with id: " + id);
        }

        return super.put(path, payload);
    }

    @Override
    public ApiResponse delete(String path) throws SQLException {
        Matcher idMatch = idPattern.matcher(path);
        if (idMatch.matches()) {
            int id = Integer.parseInt(idMatch.group("id"));
            if (storage.deleteUser(id)) {
                return ApiResponse.success(200);
            } else return ApiResponse.error(404, "There is no such user with id: " + id);
        }

        return super.delete(path);
    }
}