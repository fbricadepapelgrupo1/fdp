package com.mycompany.fdp.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mycompany.fdp.api.endpoints.UsersEndpoint;
import com.mycompany.fdp.models.ApiResponse;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

public class ApiServer {
    private final ObjectMapper objectMapper;
    private final Map<String, Endpoint> endpoints = new HashMap<>();

    public ApiServer(ObjectMapper objectMapper, int port) {
        this.objectMapper = objectMapper;

        endpoints.put("/feirapaper/resources/client/", new UsersEndpoint(objectMapper));

        try {
            ServerSocket serverSocket = new ServerSocket(port);
            System.out.println("Api Server started. Listening for connections on port: " + port);

            ThreadPoolExecutor threadPool = (ThreadPoolExecutor) Executors.newCachedThreadPool();
            while (!serverSocket.isClosed() && !threadPool.isShutdown()) {
                Socket connection = serverSocket.accept();
                threadPool.execute(() -> handleConnection(connection));
            }
        } catch (IOException e) {
            System.out.println("Failed to start Api Server: " + e.getMessage());
        }
    }

    private void handleConnection(Socket connection) {
        BufferedReader request = null;
        PrintWriter headers = null;
        BufferedOutputStream body = null;

        try {
            request = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            headers = new PrintWriter(connection.getOutputStream());
            body = new BufferedOutputStream(connection.getOutputStream());

            String input = request.readLine();
            if (input == null) return;

            String[] options = input.split("\\s");
            String method = options[0].toUpperCase();
            String path = options[1];
            String version = options[2];

            System.out.println("New connection m: " + method + " p: " + path + " v: " + version);

            boolean headerLine;
            do {
                headerLine = !request.readLine().isEmpty();
            } while (headerLine);

            StringBuilder payload = new StringBuilder();
            while (request.ready()) {
                payload.append((char) request.read());
            }

            Map.Entry<String, Endpoint> endpoint = null;
            for (Map.Entry<String, Endpoint> entry : endpoints.entrySet()) {
                if (path.startsWith(entry.getKey())) {
                    endpoint = entry;
                    break;
                }
            }

            ApiResponse apiResponse = null;
            if (endpoint != null) {
                path = path.replaceFirst(endpoint.getKey(), "");
                try {
                    switch (method) {
                        case "GET":
                            apiResponse = endpoint.getValue().get(path);
                            break;
                        case "POST":
                            apiResponse = endpoint.getValue().post(path, payload.toString());
                            break;
                        case "PUT":
                            apiResponse = endpoint.getValue().put(path, payload.toString());
                            break;
                        case "DELETE":
                            apiResponse = endpoint.getValue().delete(path);
                            break;
                    }
                } catch (SQLException e) {
                    apiResponse = ApiResponse.error(500, e.getMessage());
                    e.printStackTrace();
                }
            }

            if (apiResponse == null) {
                apiResponse = ApiResponse.error(404, "Not a valid api endpoint.");
            }

            byte[] response = objectMapper.writeValueAsBytes(apiResponse);
            if (apiResponse.getHttpCode() == 200) {
                headers.println(version + " 200 OK");
            } else if (apiResponse.getHttpCode() == 201) {
                headers.println(version + " 201 Created");
            } else if (apiResponse.getHttpCode() == 400) {
                headers.println(version + " 400 Bad Request");
            } else if (apiResponse.getHttpCode() == 404) {
                headers.println(version + " 404 Not Found");
            } else if (apiResponse.getHttpCode() == 500) {
                headers.println(version + " 500 Internal Server Error");
            }

            headers.println("Date: " + new Date());
            headers.println("Content-type: application/json");
            headers.println("Content-length: " + response.length);
            headers.println();
            headers.flush();

            body.write(response);
            body.flush();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (body != null) {
                    body.close();
                }
                if (headers != null) {
                    headers.close();
                }
                if (request != null) {
                    request.close();
                }
                connection.close();
            } catch (IOException e) {
                System.err.println("Error closing connection: " + e.getMessage());
            }
        }
    }
}