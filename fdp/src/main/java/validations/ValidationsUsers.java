package validations;

import com.mycompany.fdp.database.DB_Con;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Scanner;

/**
 *
 * @author Renato
 */
public class ValidationsUsers {

    Scanner ler = new Scanner(System.in);

    /**
     * Valida se o campo está preenchido ou não
     *
     * @param word
     * @return
     */
    public boolean validarCampoVazioString(String word) {
        while (word.isEmpty()) {
            System.out.println("Campo vazio");
            word = ler.nextLine();
        }
        return false;
    }

    /**
     * Validação de username na base de dados
     *
     * @param user_Name
     * @return
     */
    public boolean validarExisteUsername(String user_Name) {
        String userVerification = "SELECT UserName FROM Users WHERE UserName='" + user_Name + "'";
        try (Statement sttm = DB_Con.getConnection().createStatement(); ResultSet rs = sttm.executeQuery(userVerification)) {
            if (rs.next()) {
                System.out.println("Username ja existente");
                return true;
            } else {
                
                return false;

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Valida se o campo contém apenas letras
     *
     * @param word
     * @return
     */
    public boolean validarCampoLetras(String word) {
        while (!word.matches("(?<=\\s|^)[a-zA-Z]*(?=[.,;:]?\\s|$)")) {
            System.out.println("Este campo so pode ter letras");
            word = ler.nextLine();
        }
        return false;

    }

    /**
     * Valida se o campo contém apenas números
     *
     * @param word
     * @return
     */
    public boolean validarCampoNumeros(String word) {
        String regex = "[0-9]+";
        while (!word.matches(regex)) {
            System.out.println("Este campo so pode conter numeros");
            word = ler.nextLine();
        }
        return false;
    }
}
