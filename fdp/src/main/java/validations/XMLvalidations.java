/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package validations;

import com.mycompany.fdp.database.DB_Con;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author joaoleal
 */
public class XMLvalidations {
    
    /**
     * Validar se a encomenda já existe na BD @JLeal
     * @param ForeignID
     * @return 
     */
    public boolean existeEncomenda(String ForeignID) {

        String encVerification = "Select count(*) as [rowcounter] From OrderConfirmationHeader where OrderForeignID='" + ForeignID + "'";
        try (Statement sttm = DB_Con.getConnection().createStatement(); ResultSet rs = sttm.executeQuery(encVerification)) {
            rs.next();
            int counter = rs.getInt("rowcounter");

            if (counter != 0) {
                System.out.println("Encomenda já existe na BD");
                System.exit(0);
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
    /**
     * Validação de fornecedor registado na BD
     * @param fornName
     * @param fornPostal
     * @return 
     */
    public boolean existeFornecedor(String fornName, String fornPostal) {

        String FornVerification = "SELECT COUNT(UserID) AS [rowcountUser] FROM Users WHERE UName='" + fornName + "' AND PostalCode='" + fornPostal + "'";
        try (Statement sttm = DB_Con.getConnection().createStatement(); ResultSet rs = sttm.executeQuery(FornVerification)) {
            rs.next();
            //Contagem de Users com o mesmo nome e morada que são fornecedores
            int userCount = rs.getInt("rowcountUser");
            if (userCount != 0) { //Valida se o fornecedor já existe na BD e como já existe dá update à morada
                System.out.println("Fornecedor registado");
                return false;
            } else { //Fornecedor não registado, procede-se ao registo
                System.out.println("Fornecedor não registado");
                System.exit(0);
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
    
    /**
     * Validar data no XML @JLeal
     * @param dateDay
     * @param dateMonth
     * @param dateYear
     * @return 
     */
    public boolean validarDataXML(int dateDay, int dateMonth, int dateYear) {

        if ((dateDay < 1 || dateDay > 31)) {
            System.out.println("Erro na data : Dia inválido");
            System.exit(0);
            return true;
            
        }
        if ((dateMonth < 1 || dateMonth > 12)) {
            System.out.println("Erro na data : Mês inválido");
            System.exit(0);
            return true;
        }
        int year = String.valueOf(dateYear).length();
        if ((year < 4)) {
            System.out.println("Erro na data : Formato Ano inválido");
            System.exit(0);
            return true;
        }
        return false;
    }

    public boolean validarCodPostal(String fornPostal) {
        String regex = "\\d{4}(-\\d{3})?";
        if (!fornPostal.matches(regex)) {
            System.out.println("Código postal inválido");
            System.exit(0);
            return true;
        }
        return false;
    }
    /**
     * Validar tipo de papel para ir buscar o paperID @JLeal  
     * @param PaperLength
     * @param PaperWidth
     * @return
     * @throws SQLException 
     */
    public int validarPaperType(int PaperLength, int PaperWidth) throws SQLException {
        int PaperID;
        if ((PaperLength == 0) || (PaperWidth == 0)) {
            System.out.println("As dimensões do papel não estão preenchidas!");
            System.exit(0);
            return 0;
        } else {
            String checkPaper = "SELECT PaperID FROM PAPERTYPE WHERE  PAPERLENGTH=" + PaperLength + " AND PAPERWIDTH=" + PaperWidth + ";";
            try (Statement stm = DB_Con.getConnection().createStatement();
                    ResultSet rs = stm.executeQuery(checkPaper)) {
                rs.next();
                PaperID = (rs.getInt("PaperID"));
            }
            return PaperID;
        }
    }
/**
 * valida se o nome não está vazio
 * @param filename
 * @return 
 */
    public boolean validarFileName(String filename) {
        if (filename.isEmpty()) {
            System.out.println("O caminho inserido não é válido");
            System.exit(0);
            return true;
        }
        return false;
    }

}
