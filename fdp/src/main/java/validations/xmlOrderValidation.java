/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package validations;

import static com.mycompany.fdp.Model.OrderConfirmationHeader.Ulins;

/**
 *
 * @author rui
 */
public class xmlOrderValidation {

    public xmlOrderValidation() {
    }
    /**
     * valida ser o input e retorna o valor correspondente.
     * @param input
     * @return 
     */
    public int validar(char input) {
        switch (input) {
            case 'Y':
                return 1;
            case 'N':
                return 2;
            default:
                return 0;
        }
    }

}
