package com.mycompany.fdp.api.endpoints;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mycompany.fdp.api.Endpoint;
import com.mycompany.fdp.models.ApiResponse;
import com.mycompany.fdp.models.Stock;
import com.mycompany.fdp.storage.StockStorage;

import java.sql.SQLException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StockEndpoint extends Endpoint {
    private final StockStorage storage = new StockStorage();
    private final Pattern idPattern = Pattern.compile("^(?<id>[0-9]+)/$");

    private final ObjectMapper objectMapper;
    public StockEndpoint(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Override
    public ApiResponse get(String path) throws SQLException {
        if (path.isEmpty()) {
            return ApiResponse.success(200, storage.getAllStock());
        }

        Matcher idMatch = idPattern.matcher(path);
        if (idMatch.matches()) {
            int id = Integer.parseInt(idMatch.group("id"));
            Stock stock = storage.getStock(id);
            if (stock != null) {
                return ApiResponse.success(200, stock);
            } else return ApiResponse.error(404, "There is no such user with id: " + id);
        }

        return super.get(path);
    }
}
