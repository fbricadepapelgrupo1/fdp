package com.mycompany.fdp.api;

import com.mycompany.fdp.models.ApiResponse;

import java.sql.SQLException;

public abstract class Endpoint {
    public ApiResponse get(String path) throws SQLException {
        return null;
    }

    public ApiResponse post(String path, String payload) throws SQLException {
        return null;
    }

    public ApiResponse put(String path, String payload) throws SQLException {
        return null;
    }

    public ApiResponse delete(String path) throws SQLException {
        return null;
    }
}