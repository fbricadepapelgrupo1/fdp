package com.mycompany.fdp.api.endpoints;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mycompany.fdp.api.Endpoint;
import com.mycompany.fdp.models.ApiResponse;
import com.mycompany.fdp.models.Order;
import com.mycompany.fdp.storage.OrderStorage;

import java.sql.SQLException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class OrderEndPoint extends Endpoint {
    private final OrderStorage storage = new OrderStorage();
    private final Pattern idPattern = Pattern.compile("^(?<id>[0-9]+)/$");

    private final ObjectMapper objectMapper;
    public OrderEndPoint(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Override
    public ApiResponse get(String path) throws SQLException {
        if (path.isEmpty()) {
            return ApiResponse.success(200, storage.getAllOrders());
        }
        return super.get(path);
    }
    @Override
    public ApiResponse post(String path, String payload) throws SQLException {
        if (path.isEmpty()) {
            try {
                Order parsedOrder = objectMapper.readValue(payload, Order.class);
                Order createdOrder = storage.createOrder(parsedOrder);
                return ApiResponse.success(201, createdOrder);
            } catch (JsonProcessingException e) {
                return ApiResponse.error(400, e.getOriginalMessage());
            }
        }

        return super.post(path, payload);
    }
}