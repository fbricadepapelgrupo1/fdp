package com.mycompany.fdp.storage;

import com.mycompany.fdp.models.Order;
import com.mycompany.fdp.models.User;

import java.sql.SQLException;
import java.util.List;

public class OrderStorage extends Storage<Order> {

    public RowMapper<Order> getRowMapper() {
        return resultSet -> new Order(
                resultSet.getString("OrderForeignID"),
                resultSet.getInt("TotalOrderPrice"),
                resultSet.getString("OrderDate"),
                resultSet.getInt("OrderConfirmationID"),
                resultSet.getInt("ProductIdentifierBuyer"),
                resultSet.getInt("ProductIdentifierSupplier"),
                resultSet.getInt("TaxPercent"),
                resultSet.getString("TaxLocation"),
                resultSet.getString("TaxType"),
                resultSet.getInt("PricePerUnit"),
                resultSet.getInt("QuantityReam"),
                resultSet.getInt("QuantitySheet"),
                resultSet.getInt("QuantityKilogram"),
                resultSet.getInt("TotalLinePrice"),
                resultSet.getInt("GramsPerSquareMeter"),
                resultSet.getInt("OrderStatus"),
                resultSet.getInt("PaperID")
        );
    }

    public List<Order> getAllOrders() throws SQLException {
        return listRows("SELECT * FROM OrderConfirmationHeader INNER JOIN OrderConfirmationLine on OrderConfirmationLine.OrderConfirmationID = OrderConfirmationHeader.OrderConfirmationID");
    }

    public Order createOrder(Order order) throws SQLException {
        return getRow(
            "INSERT INTO OrderConfirmationHeader(OrderConfirmationID,OrderDate,UserID,TotalOrderPrice,OrderForeignID) OUTPUT INSERTED.* VALUES (?,?,?,?,?)",
            order.getOrderConfirmationID(), order.getOrderDate(), "2016", order.getTotalOrderPrice(),order.getOrderForeignID(),
            "INSERT INTO OrderConfirmationLine(OrderConfirmationID,ProductIdentifierBuyer,ProductIdentifierSupplier,TaxPercent,TaxLocation,TaxType,PricePerUnit,QuantityReam,QuantitySheet,QuantityKilogram,TotalLinePrice,GramsPerSquareMeter,OrderStatus,PaperID) OUTPUT INSERTED.* VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?) ",
            order.getOrderConfirmationID(),order.getProductIdentifierBuyer(),order.getProductIdentifierSupplier(),order.getTaxPercent(),order.getTaxLocation(),order.getTaxType(),order.getPricePerUnit(),order.getQuantityReam(),"null","null",order.getTotalLinePrice(),order.getGramsPerSquareMeter(),order.getOrderStatus(),order.getPaperID()
        );
    }
}