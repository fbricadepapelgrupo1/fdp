package com.mycompany.fdp.storage;

import com.mycompany.fdp.models.Stock;
import com.mycompany.fdp.models.User;

import java.sql.SQLException;
import java.util.List;

public class StockStorage extends Storage<Stock> {
     //rowmapper converte uma linha da bd para um usuario
    @Override
    public RowMapper<Stock> getRowMapper() {
        return resultSet -> new Stock(
                resultSet.getInt("StockID"),
                resultSet.getInt("PaperID"),
                resultSet.getDouble("GramsPerSquareMeter"),
                resultSet.getDouble("QuantityKilogram"),
                resultSet.getInt("QuantitySheet"),
                resultSet.getInt("QuantityReam")
        );
    }
    public List<Stock> getAllStock() throws SQLException {
        return listRows("SELECT * FROM Stock");
    }

    public Stock getStock(int id)throws SQLException{
        return getRow("SELECT * FROM Stock WHERE StockID=?", id);
    }
}
