package com.mycompany.fdp.storage;

import com.mycompany.fdp.models.User;

import java.sql.SQLException;
import java.util.List;

public class UserStorage extends Storage<User> {
    //rowmapper converte uma linha da bd para um usuario
    @Override
    public RowMapper<User> getRowMapper() {
        return resultSet -> new User(
                resultSet.getInt("UserID"),
                resultSet.getString("UName"),
                resultSet.getString("Address1"),
                resultSet.getString("Address2"),
                resultSet.getString("City"),
                resultSet.getString("PostalCode"),
                resultSet.getString("Country"),
                resultSet.getString("UserName"),
                resultSet.getString("UserPassword"),
                resultSet.getInt("UserType")
        );
    }

    public List<User> getAllUsers() throws SQLException {
        return listRows("SELECT * FROM Users");
    }

    public User getUser(int id) throws SQLException {
        return getRow("SELECT * FROM Users WHERE UserID=?", id);
    }

    public User createUser(User user) throws SQLException {
        return getRow(
                "INSERT INTO Users(UName,Address1,Address2,City,PostalCode,Country,UserName,UserPassword,UserType) OUTPUT INSERTED.* VALUES (?,?,?,?,?,?,?,?,?)",
                user.getName(), user.getAddress1(), user.getAddress2(), user.getCity(), user.getPostalCode(), user.getCountry(),
                user.getUserName(), user.getPassword(), user.getUserType()
        );
    }

    public boolean updateUser(User original, User user) throws SQLException {
        return updateRow(
                "UPDATE Users SET UName=?,Address1=?,Address2=?,City=?,PostalCode=?,Country=?,UserName=?,UserPassword=?,UserType=? WHERE UserID=?",
                user.getName(), user.getAddress1(), user.getAddress2(), user.getCity(), user.getPostalCode(), user.getCountry(),
                user.getUserName(), user.getPassword(), user.getUserType(), original.getId()
        ) > 0;
    }

    public boolean deleteUser(int id) throws SQLException {
        return updateRow("DELETE FROM Users WHERE UserID=?", id) > 0;
    }
}