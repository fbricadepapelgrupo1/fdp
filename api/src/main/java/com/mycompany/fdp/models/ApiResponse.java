package com.mycompany.fdp.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ApiResponse {
    private String status;
    private String error;
    private Object response;

    @JsonIgnore
    private int httpCode;

    public static ApiResponse error(int httpCode, String error) {
        ApiResponse apiResponse = new ApiResponse();
        apiResponse.status = "error";
        apiResponse.httpCode = httpCode;
        apiResponse.error = error;
        return apiResponse;
    }

    public static ApiResponse success(int httpCode) {
        return success(httpCode, null);
    }

    public static ApiResponse success(int httpCode, Object response) {
        ApiResponse apiResponse = new ApiResponse();
        apiResponse.status = "success";
        apiResponse.httpCode = httpCode;
        apiResponse.response = response;
        return apiResponse;
    }

    public String getStatus() {
        return status;
    }

    public String getError() {
        return error;
    }

    public Object getResponse() {
        return response;
    }

    public int getHttpCode() {
        return httpCode;
    }

    @Override
    public String toString() {
        return "ApiResponse{" +
                "status='" + status + '\'' +
                ", error='" + error + '\'' +
                ", response=" + response +
                ", httpCode=" + httpCode +
                '}';
    }
}