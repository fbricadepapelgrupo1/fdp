package com.mycompany.fdp.models;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Order {
    private String OrderForeignID;

    public String getOrderForeignID() {
        return OrderForeignID;
    }

    public void setOrderForeignID(String OrderForeignID) {
        this.OrderForeignID = OrderForeignID;
    }
    private int TotalOrderPrice;
    private String OrderDate;
    private int OrderConfirmationID;
    private int ProductIdentifierBuyer;
    private int ProductIdentifierSupplier;
    private int TaxPercent;
    private String TaxLocation;
    private String TaxType;
    private int PricePerUnit;
    private int QuantityReam;
    private int QuantitySheet;
    private int QuantityKilogram;
    private int TotalLinePrice;
    private int GramsPerSquareMeter;
    private int OrderStatus;
    private int PaperID;

    @JsonCreator
    public Order(
            @JsonProperty("OrderForeignID") String OrderForeignID,
            @JsonProperty("TotalOrderPrice") int TotalOrderPrice,
            @JsonProperty("OrderDate") String OrderDate,
            @JsonProperty("OrderConfirmationID") int OrderConfirmationID,
            @JsonProperty("ProductIdentifierBuyer") int ProductIdentifierBuyer,
            @JsonProperty("ProductIdentifierSupplier") int ProductIdentifierSupplier,
            @JsonProperty("TaxPercent") int TaxPercent,
            @JsonProperty("TaxLocation") String TaxLocation,
            @JsonProperty("TaxType") String TaxType,
            @JsonProperty("PricePerUnit") int PricePerUnit,
            @JsonProperty("QuantityReam") int QuantityReam,
            @JsonProperty("QuantitySheet") int QuantitySheet,
            @JsonProperty("QuantityKilogram") int QuantityKilogram,
            @JsonProperty("TotalLinePrice") int TotalLinePrice,
            @JsonProperty("GramsPerSquareMeter") int GramsPerSquareMeter,
            @JsonProperty("OrderStatus") int OrderStatus,
            @JsonProperty("PaperID") int PaperID
    ) {
        this.OrderForeignID = OrderForeignID;
        this.TotalOrderPrice = TotalOrderPrice;
        this.OrderDate = OrderDate;
        this.OrderConfirmationID = OrderConfirmationID;
        this.ProductIdentifierBuyer = ProductIdentifierBuyer;
        this.ProductIdentifierSupplier = ProductIdentifierSupplier;
        this.TaxPercent = TaxPercent;
        this.TaxLocation = TaxLocation;
        this.TaxType = TaxType;
        this.PricePerUnit=PricePerUnit;
        this.QuantityReam=QuantityReam;
        this.QuantitySheet=QuantitySheet;
        this.QuantityKilogram=QuantityKilogram;
        this.TotalLinePrice=TotalLinePrice;
        this.GramsPerSquareMeter=GramsPerSquareMeter;
        this.OrderStatus=OrderStatus;
        this.PaperID=PaperID;
    }

    public int getTotalOrderPrice() {
        return TotalOrderPrice;
    }

    public void setTotalOrderPrice(int TotalOrderPrice) {
        this.TotalOrderPrice = TotalOrderPrice;
    }

    public String getOrderDate() {
        return OrderDate;
    }

    public void setOrderDate(String OrderDate) {
        this.OrderDate = OrderDate;
    }

    public int getOrderConfirmationID() {
        return OrderConfirmationID;
    }

    public void setOrderConfirmationID(int OrderConfirmationID) {
        this.OrderConfirmationID = OrderConfirmationID;
    }

    public int getProductIdentifierBuyer() {
        return ProductIdentifierBuyer;
    }

    public void setProductIdentifierBuyer(int ProductIdentifierBuyer) {
        this.ProductIdentifierBuyer = ProductIdentifierBuyer;
    }

    public int getProductIdentifierSupplier() {
        return ProductIdentifierSupplier;
    }

    public void setProductIdentifierSupplier(int ProductIdentifierSupplier) {
        this.ProductIdentifierSupplier = ProductIdentifierSupplier;
    }

    public int getTaxPercent() {
        return TaxPercent;
    }

    public void setTaxPercent(int TaxPercent) {
        this.TaxPercent = TaxPercent;
    }

    public String getTaxLocation() {
        return TaxLocation;
    }

    public void setTaxLocation(String TaxLocation) {
        this.TaxLocation = TaxLocation;
    }

    public String getTaxType() {
        return TaxType;
    }

    public void setTaxType(String TaxType) {
        this.TaxType = TaxType;
    }

    public int getPricePerUnit() {
        return PricePerUnit;
    }

    public void setPricePerUnit(int PricePerUnit) {
        this.PricePerUnit = PricePerUnit;
    }

    public int getQuantityReam() {
        return QuantityReam;
    }

    public void setQuantityReam(int QuantityReam) {
        this.QuantityReam = QuantityReam;
    }

    public int getQuantitySheet() {
        return QuantitySheet;
    }

    public void setQuantitySheet(int QuantitySheet) {
        this.QuantitySheet = QuantitySheet;
    }

    public int getQuantityKilogram() {
        return QuantityKilogram;
    }

    public void setQuantityKilogram(int QuantityKilogram) {
        this.QuantityKilogram = QuantityKilogram;
    }

    public int getTotalLinePrice() {
        return TotalLinePrice;
    }

    public void setTotalLinePrice(int TotalLinePrice) {
        this.TotalLinePrice = TotalLinePrice;
    }

    public int getGramsPerSquareMeter() {
        return GramsPerSquareMeter;
    }

    public void setGramsPerSquareMeter(int GramsPerSquareMeter) {
        this.GramsPerSquareMeter = GramsPerSquareMeter;
    }

    public int getOrderStatus() {
        return OrderStatus;
    }

    public void setOrderStatus(int OrderStatus) {
        this.OrderStatus = OrderStatus;
    }

    public int getPaperID() {
        return PaperID;
    }

    public void setPaperID(int PaperID) {
        this.PaperID = PaperID;
    }

    @Override
    public String toString() {
        return "Order{" + "OrderForeignID=" + OrderForeignID + ", TotalOrderPrice=" + TotalOrderPrice + ", OrderDate=" + OrderDate + ", OrderConfirmationID=" + OrderConfirmationID + ", ProductIdentifierBuyer=" + ProductIdentifierBuyer + ", ProductIdentifierSupplier=" + ProductIdentifierSupplier + ", TaxPercent=" + TaxPercent + ", TaxLocation=" + TaxLocation + ", TaxType=" + TaxType + ", PricePerUnit=" + PricePerUnit + ", QuantityReam=" + QuantityReam + ", QuantitySheet=" + QuantitySheet + ", QuantityKilogram=" + QuantityKilogram + ", TotalLinePrice=" + TotalLinePrice + ", GramsPerSquareMeter=" + GramsPerSquareMeter + ", OrderStatus=" + OrderStatus + ", PaperID=" + PaperID + '}';
    }
    

}
