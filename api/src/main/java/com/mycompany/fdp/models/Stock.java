
package com.mycompany.fdp.models;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Stock {
    private final int id;
    private int paperId;
    private double gramsSqrMeter;
    private double quantityKG;
    private int quantitySheet;
    private int quantityReam;

    @JsonCreator
    public Stock(
        @JsonProperty("id") int id,
        @JsonProperty("paperId") int paperId,
        @JsonProperty("gramsSqrMeter") double gramsSqrMeter,
        @JsonProperty("quantityKG") double quantityKG,
        @JsonProperty("quantitySheet") int quantitySheet,
        @JsonProperty("quantityReam") int quantityReam
    ){
        this.id = id;
        this.paperId = paperId;
        this.gramsSqrMeter = gramsSqrMeter;
        this.quantityKG = quantityKG;
        this.quantitySheet = quantitySheet;
        this.quantityReam = quantityReam;
}
    public int getId() {
        return id;
    }

    public void setPaperId(int paperId) {
        this.paperId = paperId;
    }
    public int getPaperID() {
        return paperId;
    }

    public void setGramsPerSquareMeter(double gramsSqrMeter) {
        this.gramsSqrMeter = gramsSqrMeter;
    }
    public double getGramsSqrMeter(){
        return gramsSqrMeter;
    }

    public void setQuantityKilogram(double quantityKG) {
        this.quantityKG = quantityKG;
    }
    public double getQuantityKilogram(){
        return quantityKG;
    }

    public  void setQuantitySheet(int quantitySheet) {
        this.quantitySheet = quantitySheet;
    }
    public int getQuantitySheet(){
        return quantitySheet;
    }

    public void setQuantityReam(int quantityReam) {
        this.quantityReam = quantityReam;
    }
    public int getQuantityReam(){
        return quantityReam;
    }

    @Override
    public String toString() {
        return "Stock{" +
                "id=" + id +
                ", paperId='" + paperId + '\'' +
                ", gramsSqrMeter='" + gramsSqrMeter + '\'' +
                ", quantityKG='" + quantityKG + '\'' +
                ", quantitySheet='" + quantitySheet + '\'' +
                ", quantityReam='" + quantityReam  +
                '}';
    }
    
}
