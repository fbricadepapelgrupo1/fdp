package com.mycompany.fdp.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DB {
    private static Connection connection;

    public static Connection getConnection() throws SQLException {
        if (connection != null && !connection.isClosed()) {
            return connection;
        }
        return openConnection();
    }

    private static final String url = "jdbc:sqlserver://ctespbd.dei.isep.ipp.pt:1433;database=grupo7";
    private static final String user = "grupo7";
    private static final String pass = "#fdp_19";
    public static synchronized Connection openConnection() {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("SQLServer driver is not available", e);
        }

        try {
            return connection = DriverManager.getConnection(url, user, pass);
        } catch (SQLException e) {
            throw new RuntimeException("Failed to open a database connection", e);
        }
    }
}