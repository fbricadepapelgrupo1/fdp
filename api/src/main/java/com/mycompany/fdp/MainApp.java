package com.mycompany.fdp;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.mycompany.fdp.api.ApiServer;
import com.mycompany.fdp.storage.StockStorage;
import com.mycompany.fdp.database.DB;


public class MainApp {
    //coverter de java para json e vice versa
    private static final ObjectMapper objectMapper = new ObjectMapper()
            .setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.NONE)
            .setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY)
            .enable(SerializationFeature.INDENT_OUTPUT);

    public static void main(String[] args) {
        DB.openConnection(); // abrir conexao a bd antes de tudo
       new ApiServer(objectMapper,8000);
        //System.out.println(new StockStorage().getStock(1));
    }
}
